# QIS
Quasi-isometric stiffening C++ implementation

# Compile and run:
```sh
mkdir build && cd build && cmake .. && cmake --build . -j &&
./stiffening2d ../hand.obj &&
./stiffening3d ../wrench-rest.mesh ../wrench-init.mesh ../wrench-lock.txt
```

