#include <iostream>

#include <ultimaille/all.h>

using namespace UM;

double triangle_area_2d(vec2 a, vec2 b, vec2 c) {
    return .5*((b.y-a.y)*(b.x+a.x) + (c.y-b.y)*(c.x+b.x) + (a.y-c.y)*(a.x+c.x));
}

int main(int argc, char** argv) {
    Triangles m;
    read_by_extension(argv[1], m);
    std::cerr << m.nverts() << std::endl;

    {
        vec3 bbmin, bbmax;
        const double boxsize = 1.;
        BBox3 bb = m.points.util.bbox();
        double maxside = std::max(bb.max.x-bb.min.x, bb.max.y-bb.min.y);
        for (vec3 &p : m.points)
            p = (p - (bb.max+bb.min)/2.)*boxsize/maxside + vec3(1,1,1)*boxsize/2;
    }

//  std::vector<bool> to_delete(m.nverts(), true);
//  for (int v : vert_iter(m)) {
//      if (rand()%4==0) to_delete[v] = false;
//  }

//  m.delete_vertices(to_delete);

    for (vec3 &p : m.points) {
        p.x *= 9*p.x;
        p.y *= 9*p.y;
        p.z *= 9*p.z;
    }


/*
    double scale =  .3;
    Triangles mi;

    PointAttribute<vec2> tex_coord("tex_coord", attr, mi);
    FacetAttribute<double> fi("f", attr, mi);

#if 0
    {
        SurfaceConnectivity fec(mi);
        int n = mi.nfacets();
        DisjointSet ds(n);
        for (int c : corner_iter(mi)) {
            int oppc = fec.opposite(c);
            if (oppc<0) continue;
            ds.merge(fec.facet(c), fec.facet(oppc));
        }
        std::vector<int> id2setid;
        ds.get_sets_id(id2setid);

        int maxid = 0;
        for (int f : facet_iter(mi)) {
            if (ds.setsize(f)>ds.setsize(maxid))
                maxid = f;
        }

        std::vector<bool> to_kill(mi.nverts(), false);
        for (int f : facet_iter(mi)) {
            if (id2setid[f]==id2setid[maxid]) continue;
            for (int lv : {0,1,2})
                to_kill[mi.vert(f, lv)] = true;
        }
      mi.delete_vertices(to_kill);
    }
#endif

#if 1
    for (int t : facet_iter(mi)) {
        vec2 A,B,C;
        mi.util.project(t, A, B, C);
        mat<2,2> ST = {{B-A, C-A}};
        mat<3,2> ref_tri = mat<3,2>{{ {-1,-1},{1,0},{0,1} }}*ST.invert_transpose();

        mat<2,2> J0;
        for (int i : {0,1,2})
            for (int d : {0,1}) {
                int v = mi.vert(t,i);
                J0[d] += ref_tri[i]*tex_coord[v][d];
            }
        {
            double f0 = (J0[0]*J0[0] + J0[1]*J0[1])/(2.*J0.det());
            double g0 = (1+J0.det()*J0.det())/(2.*J0.det());
            fi[t] = (.5*f0 + .5*g0);
        }
     }
#endif

    for (int v : vert_iter(mi))
        mi.points[v] = {tex_coord[v].x, tex_coord[v].y, 0};



    write_by_extension("2d.geogram", mi, {{}, {{"f", fi.ptr}}, {}});

//  for (int v : vert_iter(mi)) {
//      mi.points[v] = {tex_coord[v].x, tex_coord[v].y, fi[v]};
//  }


    Polygons mo;
    PointAttribute<double> fo(mo);
    PointAttribute<bool> selection(mo);
    int n = mi.nfacets();
    mo.points.create_points(n*3);
    mo.create_facets(n,3);



    for (int i : range(n*3)) {
        mo.points[i] = mi.points[mi.vert(i/3, i%3)];
//      mo.points[i].z = fi[i/3]*scale;
//    if (i==257 || i==17434 || i==18139 || i==25684 || i==26464||i==2935 || i==6394||i==6516||i==10173) {
//          std::cerr << mi.vert(i/3, i%3) << std::endl;
//      selection[i] = true;
//    }
//      vec2 p = tex_coord[mi.vert(i/3, i%3)];
//      mo.points[i] = {p.x, p.y, fi[i/3]};
        mo.vert(i/3, i%3) = i;
    }
    for (int t : facet_iter(mi)) {
        for (int i : {0,1,2}) {
            mo.points[t*3+i].z = fi[t]*scale;
//          std::cerr << fi[t] << std::endl;
        }
    }


    SurfaceConnectivity fec(mi);
    for (int t : facet_iter(mi)) {
        for (int i : {0,1,2}) {
            mo.points[t*3+i].z = fi[t]*scale;
            fo[t*3+i] = fi[t];

            int oppc = fec.opposite(t*3+i);
            if (oppc<0 || fec.from(t*3+i) > fec.to(t*3+i)) continue;
            int oppf = fec.facet(oppc);

//          std::cerr << t << " " << i << std::endl;
            int offv = mo.points.create_points(4);
            int offf = mo.create_facets(1, 4);
            for (int i : range(4))
                mo.vert(offf, i) = offv+i;

            mo.points[offv+0] = mi.points[fec.from(t*3+i)];
            mo.points[offv+1] = mi.points[fec.to(t*3+i)];
            mo.points[offv+2] = mi.points[fec.from(oppc)];
            mo.points[offv+3] = mi.points[fec.to(oppc)];
            for (int i : range(2)) {
                mo.points[offv+i].z = fi[t]*scale;
                mo.points[offv+i+2].z = fi[oppf]*scale;
                fo[offv+i] = fi[t];
                fo[offv+i+2] = fi[oppf];
            }
        }
    }

#if 0
    { // colocate vertices
        std::vector<int> old2new;
        colocate(*mo.points.data, old2new, 1e-3);
        for (int f : facet_iter(mo))
            for (int lv : range(mo.facet_size(f)))
                mo.vert(f, lv) = old2new[mo.vert(f, lv)];
       std::cerr << mo.nverts() << std::endl;
        mo.delete_isolated_vertices();
       std::cerr << mo.nverts() << std::endl;
    }
#endif


*/
    write_by_extension("f.geogram", m);
    return 0;
}

