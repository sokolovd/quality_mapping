import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker
import sys

plt.rcParams["font.family"] = "serif"
plt.rcParams["mathtext.fontset"] = "dejavuserif"
plt.rcParams['text.usetex'] = True
plt.rc('font', size=24)
plt.rcParams['hatch.color'] = 'white'
#plt.rcParams['hatch.linewidth'] = 20.0
plt.rcParams['figure.autolayout'] = True
plt.rcParams["legend.framealpha"] = 1.

fig, ax = plt.subplots(1, figsize=(10.80/2,10.80/2),dpi=200)

[name, nverts, niter,time] = np.loadtxt("2d-stiffening-timings.csv", delimiter=',',  dtype={'names': ('id', '#verts', '#iter', 'time'), 'formats': (np.str, np.int, np.int, np.float)}, unpack=True, skiprows=1)

#fig = plt.figure()
ax = plt.gca()
ax.plot(nverts, time, 'o', markeredgecolor='none', markersize=2, alpha=.5)
ax.set_yscale('log')
ax.set_xscale('log')

#ax.set_title("2d dataset")
ax.set_title("Running times, 2D")

ax.set_ylabel('time, seconds')
ax.set_xlabel('\# vertices')
#ax.set_xlim(10**(-25.5), 1.)
#ax.set_xlim(right=1.)

#ax.set_xticks(np.arange(0, 10**5, 5))

ax.set_xticks([10**2, 10**3, 10**4, 10**5])
#ax.set_yticks([10**(-2), 10**(-1), 10**0, 10**1, 10**2, 10**3])
#ax.set_yticks([10**(-2), 10**(-1), 10**0, 10**1, 10**2])
#ax.set_xticks([10**3, 10**4, 10**5])
#ax.set_xticks([10**2, 10**3, 10**4, 10**5])
#ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())


plt.tight_layout()
plt.savefig("timings-2d.png")

print(sum(time))

#ax.legend()
#plt.show()


