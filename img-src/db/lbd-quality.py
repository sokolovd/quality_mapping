import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker
from matplotlib.collections import LineCollection
import sys

plt.rcParams["font.family"] = "serif"
plt.rcParams["mathtext.fontset"] = "dejavuserif"
plt.rcParams['text.usetex'] = True
plt.rc('font', size=24)
plt.rcParams['hatch.color'] = 'white'
#plt.rcParams['hatch.linewidth'] = 20.0
plt.rcParams['figure.autolayout'] = True
plt.rcParams["legend.framealpha"] = 1.

fig, ax = plt.subplots(1, figsize=(10.80/2,10.80/2),dpi=200)

[lbdf,lbddet,lbdcond,qisf,qisdet,qiscond] = np.loadtxt('lbd.csv', delimiter=',', unpack=True, skiprows=1)

badid = []
for i in range(len(lbdcond)):
        if lbdcond[i]<qiscond[i]:
            badid.append(i)
print(len(badid))


X = []
Y = []
cnt=0
for i in range(len(lbdcond)):
    if lbdcond[i]>1.515 and qiscond[i]<1.515:
        cnt+=1
    if i in badid: continue
    X.append(qiscond[i])
    Y.append(lbdcond[i])

print(cnt)

ax = plt.gca()
line = [(1, 1), (10**(4), 10**(4))]
line1 = [(1, 1.5), (10**(4), 1.5)]
line2 = [(1.5,1), (1.5,10**(4))]
lc = LineCollection([line,line1, line2], color=["gray"],lw=.5)
plt.gca().add_collection(lc)


ax.plot(X, Y, 'o', markeredgecolor='none', markersize=2, alpha=.5)
#ax.plot(nverts, time, 'o', markeredgecolor='none', markersize=2, alpha=.5)
#ax.plot(nverts2, time2, 'o', markeredgecolor='none', markersize=2, alpha=.9)
ax.set_yscale('log')
ax.set_xscale('log')

plt.text(1.2, 60, '1380 fails')
plt.text(30, 1.2, '0 fails')

ax.set_title("$\max \sigma_1(J)/\sigma_2(J)$")

ax.set_ylabel('LBD')
ax.set_xlabel('QIS')
ax.set_xlim(1,10**(2))
ax.set_ylim(1,10**(2))
#ax.set_yticks([10**(-10), 10**(-5), 10**0])
#ax.set_xticks([10**(-10), 10**(-5), 10**0])

#for i in filtid:
#    if amipsmaxdist[i]<tmaxdist[i]:
#        print(i)
#        print(filename[i],idpmaxdist[i],tmaxdist[i])

#ax.set_xticks(np.arange(0, 10**5, 5))

#ax.set_xticks([10**2, 10**3, 10**4, 10**5])
#ax.set_yticks([10**(-2), 10**(-1), 10**0, 10**1, 10**2])
#ax.set_xticks([10**3, 10**4, 10**5])
#ax.set_xticks([10**2, 10**3, 10**4, 10**5])
#ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())

plt.tight_layout()
plt.savefig("lbd.png")

#ax.legend()
#plt.show()

