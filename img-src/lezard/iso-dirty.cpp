#include <ultimaille/all.h>
#include <ultimaille/algebra/quaternion.h>
#include <string>

#define FOR(i, n) for(int i = 0; i < (int) n; i++)
using namespace UM;
namespace Dirty {
    const std::string prefix = "./file";
    inline double ave_edge_size(const Surface& m) {
        SurfaceConnectivity fec(m);
        double res = 0;
        FOR(c, m.ncorners())  res += fec.geom(c).norm();
        return res / double(m.ncorners());
    }
    inline Quaternion align_with_uv(vec3 u, vec3 v) {
        v.normalize();
        u.normalize();
        if (u * v < -.99999) { Quaternion res;  res.v = vec3(1, 0, 0); res.w = 0;  return res; }
        if (std::abs(u * v) > .99999)  return Quaternion();
        vec3 inbetwen_uv(v + u);
        inbetwen_uv.normalize();
        Quaternion res;
        res.w = v * inbetwen_uv; // scalar product with (1,0,0) divided by norm
        res.v = cross(inbetwen_uv, v); // cross product with (1,0,0) 
        return res;
    }

    void add_tube(Polygons& out_mesh, double value, const vec3& A, const vec3& B,double diameter=1., int resolution = 10) {

        int offv = out_mesh.points.create_points(resolution * 2);
        FOR(v, resolution) 
            out_mesh.points[offv + v] = .5 * diameter * vec3(
                cos(2. * M_PI * double(v) / double(resolution)),
                sin(2. * M_PI * double(v) / double(resolution)),
                0);
        FOR(v, resolution) out_mesh.points[offv + resolution + v] = out_mesh.points[offv + v] + vec3(0, 0, 1.);
        int offf = out_mesh.create_facets(resolution, 4);
        FOR(f, resolution) {
            out_mesh.facets[out_mesh.offset[offf + f] + 0] = offv + f;
            out_mesh.facets[out_mesh.offset[offf + f] + 1] = offv + (f + 1) % resolution;
            out_mesh.facets[out_mesh.offset[offf + f] + 2] = offv + (f + 1) % resolution + resolution;
            out_mesh.facets[out_mesh.offset[offf + f] + 3] = offv + f + resolution;
        }

        vec3 n = B - A;
        double l = std::max(1e-5, n.norm());
        n = n / l;

        for (int v = offv; v < offv + 2 * resolution; v++)  out_mesh.points[v][2] *= l;

        Quaternion quat = align_with_uv(vec3(0, 0, 1), n);
        auto M = quat.rotation_matrix();
        for (int v = offv; v < offv + 2 * resolution; v++)
            out_mesh.points[v] = M * out_mesh.points[v] + A;
    }


    void drop_polyline_edge(const PolyLine& m,int resolution,double diameter) {
        static int num = 0;
        Polygons outm;
        FacetAttribute<double> attr(outm);
        FOR(e, m.nsegments()) {
            add_tube(outm, 0, m.points[m.vert(e, 0)], m.points[m.vert(e, 1)],diameter,resolution);
        }
        write_by_extension(prefix + std::to_string(num++) +".geogram", outm, { {},{},{} });
    }

    void extact_uv(Triangles& m, const CornerAttribute<vec2>& U, double scale = .05) {
        static int num = 0; 
        SurfaceConnectivity fec(m);
        for (auto d : { 0,1 }) {
            PolyLine poly;
            FOR(f,m.nfacets()){
                double minv = 1e20;
                double maxv = -1e20;
                FOR(lh,3){
                    int cir = m.corner(f, lh);
                    minv = std::min(minv, U[cir][d]);
                    maxv = std::max(maxv, U[cir][d]);
                }
                for (double iso = std::floor(minv); iso < std::ceil(maxv) + 1; iso += 1.) {
                    std::vector<vec3> pts;
                    FOR(lh, 3) {
                        int cir = m.corner(f, lh);
                        if (std::abs(U[fec.next(cir)][d] - U[cir][d]) < 1e-3)continue;
                        double c = (iso - U[cir][d]) / (U[fec.next(cir)][d] - U[cir][d]);
                        if (c < -.0001) continue;
                        if (c > 1.0001) continue;
                        pts.push_back((1. - c) * fec.m.points[fec.from(cir)] + c * fec.m.points[fec.to(cir)]);
                    }
                    if (pts.size() == 3) {
                        if ((pts[0] - pts[1]).norm2() < 1e-5) std::swap(pts[0], pts[2]);
                        pts.pop_back();
                    }
                    if (pts.size() == 2) {
                        int offs = poly.create_segments(1);
                        int offv = poly.points.create_points(2);
                        FOR(e, 2) poly.vert(offs, e) = offv + e;
                        FOR(e, 2) poly.points[offv + e] = pts[e];
                    }
                }
            }
            double ave = ave_edge_size(m);
            drop_polyline_edge(poly,10,ave*scale);
            write_by_extension(prefix + "wire"+std::to_string(num++) + ".geogram", poly, { {},{} });
        }
    }

};




int main(int argc, char** argv) {
    char* filename = "C:\\NICO\\tmp\\2covering.geogram";
    if (argc == 2) filename = argv[1];

    // do your stuff
    std::cerr << "Load file " << filename << std::endl;
    Triangles m;
    SurfaceAttributes tmp = read_by_extension(filename, m);
    write_by_extension(Dirty::prefix+".geogram", m, tmp);

    auto& corner_attribs = tmp.corners;
    double scale = 1.; //for(double scale = 1.;scale<8.;scale*=2.)
    for (auto& at : corner_attribs) if (at.first == "tex_coord") {
        CornerAttribute<vec2> U("tex_coord", tmp, m);
        FOR(c, m.ncorners()) U[c] = scale * U[c];
        Dirty::extact_uv(m, U, .1 / scale);
    }


    // render the output
    system(("C:/NICO/prog/graphite/GraphiteThree/build/Windows/bin/RelWithDebInfo/graphite.exe "+ Dirty::prefix + ".geogram "+Dirty::prefix+"0.geogram  " + Dirty::prefix + "1.geogram  " + Dirty::prefix + "wire0.geogram  " + Dirty::prefix + "wire1.geogram").c_str());
    return 0;
}
