#include <iostream>

#include <ultimaille/all.h>

using namespace UM;

int main(int argc, char** argv) {
    if (argc<2) {
        std::cerr << "Usage: " << argv[0] << " filename" << std::endl;
        return 1;
    }
    std::cerr << "Checking " << argv[1] << std::endl;

    Triangles m;
    SurfaceAttributes a = read_by_extension(argv[1], m);
    PointAttribute<vec2> tex("tex_coord", a, m);

    FacetAttribute<mat<3,2>> ref_tri(m);
    for (int t : facet_iter(m)) {
        vec2 A,B,C;
        m.util.project(t, A, B, C);
        mat<2,2> ST = {{B-A, C-A}};
        ref_tri[t] = mat<3,2>{{ {-1,-1},{1,0},{0,1} }}*ST.invert_transpose();
    }

    const auto getJ = [&ref_tri, &m, &tex](int t)->mat<2, 2> { // get Jacobian matrix for triangle t
        mat<2, 2> J = {};
        for (int i : {0, 1, 2})
            for (int d : {0, 1})
                J[d] += ref_tri[t][i] * tex[m.vert(t,i)][d];
        return J;
    };


    FacetAttribute<mat2x2> R(m);
    PolyLine l;
    for (int t : facet_iter(m)) {
        mat2x2 J = getJ(t);

        auto [U,D,V] = svd2x2(J);
        R[t] = U*V.transpose();
        std::cerr << R[t] << std::endl;

        int offv = l.points.create_points(4);
        int offl = l.create_segments(2);
        for (int i : {0,1})
            for (int j : {0,1})
                l.vert(offl+i, j) = offv+i*2+j;
        vec3 bary = m.util.bary_verts(t);
        l.points[offv+0] = bary;
        l.points[offv+1] = bary+vec3{R[t][0][0], R[t][1][0], 0}*.1;
        l.points[offv+2] = bary;
        l.points[offv+3] = bary+vec3{R[t][0][1], R[t][1][1], 0}*.1;
    }
    write_by_extension("l.geogram", l);


    {
        Quads q;
        PointAttribute<double> tau(q);
        int n=128;
        q.points.create_points(n*n);
        for (int j: range(n)) for (int i:range(n)) {
            int v = j*n + i;
            q.points[v].x =  i/(double)n*1.2-.1;
            q.points[v].y =  (j/(double)n-.5)*.66;
            tex[0] = {q.points[v].x,  q.points[v].y};

            tau[v] = -1;
            for (int t: facet_iter(m)) {
                mat2x2 J = getJ(t);
                tau[v] = std::max((J-R[t]).norm(), tau[v]);
            }
        }
        for (int j: range(n-1)) for (int i:range(n-1)) {
            int off = q.create_facets(1);
            q.vert(off, 0) = i+j*n;
            q.vert(off, 1) = i+1+j*n;
            q.vert(off, 2) = i+1+(j+1)*n;
            q.vert(off, 3) = i+(j+1)*n;
        }
        write_by_extension("q.geogram", q, {{{"tau", tau.ptr}}, {}, {}});
    }



    return 0;
}

