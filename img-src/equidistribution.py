#!/usr/bin/python3
import numpy as np
import sys

print('''%!PS-Adobe-3.0
/m {moveto} def
/l {lineto} def
/s {closepath stroke} def
/S {stroke} def
/g {closepath gsave} def
/gray {.7 .7 .7 col} def
/black {0 0 0 col} def
/green {0 1 0 col} def
/red {1 0 0 col} def
/darkgreen {0 .7 0 col} def
/col {setrgbcolor fill} def
/contour {grestore 2 setlinewidth 0 setgray stroke} def
''')

P=[0,0.1293609675002844,0.2293851025566887,0.3258652588212973,0.4371276378736765,0.635065940715232,1.01]
xi = np.linspace(1/12,1-1/12,6)

scale = 100

offx = .1
offy = .3
print('black\n1 setlinewidth\nnewpath\n%f %f m' % (scale*offx, scale*offy))
for x in np.linspace(0,1,128):
    u = x - (np.cos(2 * np.pi * x) -1)*3/(8*np.pi)
    print("%f %f l" % ((x+offx)*scale, (u+offy)*scale))
print('S')

print('red\n1 setlinewidth\nnewpath\n%f %f m' % (scale*offx, scale*(-xi[0]+offy)))
for x in np.linspace(0,1,128):
    u = x - (np.cos(2 * np.pi * x) -1)*3/(8*np.pi)
    i = next(t[0] for t in enumerate(P) if t[1] > x)
    print((x,i),file=sys.stderr)
#    if i==6: i = 5
    uh = xi[i-1]
    print("%f %f l" % ((x+offx)*scale, ((u-uh)+offy)*scale))
print('S')

print("darkgreen\n1 setlinewidth")
#print(xi, file=sys. stderr)
for i in range(6):
    print('newpath\n%f %f m' % (scale*(offx+P[i]), scale*(xi[i]+offy)))
    print('%f %f l' % (scale*(offx+P[i+1]), scale*(xi[i]+offy)))
    print('S')
    if (i==5):
        continue
    print('newpath\n%f %f m' % (scale*(offx+P[i+1]), scale*(xi[i]+offy)))
    print('%f %f l' % (scale*(offx+P[i+1]), scale*(xi[i+1]+offy)))
    print('S')

print("gray\n.5 setlinewidth")
for p in np.linspace(0,1,7):
    print('newpath\n%f %f m' % (scale*(offx), scale*(p+offy)))
    print('%f %f l' % (scale*(offx-.03), scale*(p+offy)))
    print('S')

for p in P:
    print('newpath\n%f %f m' % (scale*(p+offx), scale*offy))
    print('%f %f l' % (scale*(p+offx), scale*(offy-.03)))
    print('S')

print(".1 setlinewidth\n[1 1] 0 setdash")
for p in np.linspace(0,1,7):
    print('newpath\n%f %f m' % (scale*(offx), scale*(p+offy)))
    print('%f %f l' % (scale*(offx+1), scale*(p+offy)))
    print('S')
for p in P:
    print('newpath\n%f %f m' % (scale*(p+offx), scale*offy))
    print('%f %f l' % (scale*(p+offx), scale*(offy+1)))
    print('S')

print("[] 0 setdash")


print('newpath\n%f %f m\n %f %f l %f %f l S' % (scale*(offx), scale*(offy+1), scale*(offx), scale*offy, scale*(offx+1), scale*offy))
 

offx += 1.5
print('newpath\n%f %f m' % (scale*(P[4]+offx), scale*(offy)))
for x in np.linspace(P[4],P[5],32):
     w = np.sin(2 * np.pi * x)*3/4+1
     print("%f %f l" % ((x+offx)*scale, (w+offy)*scale))
print('%f %f l' % (scale*(P[5]+offx), scale*(offy)))
print('''g
0.8 1 0.8 col
S
''')

print('newpath\n%f %f m' % (scale*(P[1]+offx), scale*(offy)))
for x in np.linspace(P[1],P[2],32):
     w = np.sin(2 * np.pi * x)*3/4+1
     print("%f %f l" % ((x+offx)*scale, (w+offy)*scale))
print('%f %f l' % (scale*(P[2]+offx), scale*(offy)))
print('''g
0.8 1 0.8 col
S
''')



print('black\n1 setlinewidth\nnewpath\n%f %f m' % (scale*offx, scale*(1+offy)))
for x in np.linspace(0,1,128):
    w = np.sin(2 * np.pi * x)*3/4+1
    print("%f %f l" % ((x+offx)*scale, (w+offy)*scale))
print('S')

print("gray\n.5 setlinewidth")
for p in P:
    print('newpath\n%f %f m' % (scale*(p+offx), scale*offy))
    print('%f %f l' % (scale*(p+offx), scale*(offy-.03)))
    print('S')
print('newpath\n%f %f m\n %f %f l %f %f l S' % (scale*(offx), scale*(offy+1.8), scale*(offx), scale*offy, scale*(offx+1), scale*offy))
 


print('''
showpage
%EOF
''')
