#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#undef NDEBUG
#include <cassert>
#include <ultimaille/all.h>

using namespace UM;

double tet_volume(const vec3 &A, const vec3 &B, const vec3 &C, const vec3 &D) {
    return ((A-D)*cross(B-D, C-D))/6.;
}

double tet_volume(const Tetrahedra &m, const int t) {
    return tet_volume(
            m.points[m.vert(t, 0)],
            m.points[m.vert(t, 1)],
            m.points[m.vert(t, 2)],
            m.points[m.vert(t, 3)]
            );
}


int main(int argc, char** argv) {
    if (3>argc) {
        std::cerr << "Usage: " << argv[0] << " rest.vtk result.vtk" << std::endl;
        return 1;
    }
    std::cerr << "Checking " << argv[1] << std::endl;

    Tetrahedra mrest, mres;
    read_by_extension(argv[1], mrest);
    read_by_extension(argv[2], mres);

    assert(mrest.ncells()==mres.ncells() && mrest.nverts()==mres.nverts());

    std::vector<double> X(mres.ncells());
    std::vector<double> Y(mres.ncells());

    CellAttribute<double> det(mres);
    for (int t : cell_iter(mrest)) {
        mat<4,3> ref_tet = {};
        for (int lf : range(4)) { // prepare the data for gradient processing: compute the normal vectors
            vec3 e0 = mrest.points[mrest.facet_vert(t, lf, 1)] - mrest.points[mrest.facet_vert(t, lf, 0)];
            vec3 e1 = mrest.points[mrest.facet_vert(t, lf, 2)] - mrest.points[mrest.facet_vert(t, lf, 0)];
            ref_tet[lf] = -(cross(e0, e1)/2.)/(3.*tet_volume(mrest, t));
        }

        mat<3,3> J = {};
        for (int i=0; i<4; i++)
            for (int d : range(3))
                J[d] += ref_tet[i]*mres.points[mres.vert(t,i)][d];

        det[t] = J.det();

        mat3x3 G = J.transpose() * J;
        mat3x3 evec;
        vec3 eval;
        eigendecompose_symmetric(G, eval, evec);

        double smax = std::sqrt(eval.x);
        double smin = std::sqrt(eval.z);

        X[t] = smax/smin;
        Y[t] = J.det();
        std::cerr << X[t] << ", " << Y[t] << std::endl;
    }


    return 0;
}


