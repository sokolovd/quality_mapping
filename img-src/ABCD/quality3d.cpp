#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#undef NDEBUG
#include <cassert>
#include <ultimaille/all.h>

using namespace UM;

/*
minscale100 0.688575, maxstretch100: 2.35251
minscale100 0.855328, maxstretch100: 1.22196
*/

int main(int argc, char** argv) {
    if (3>argc) {
        std::cerr << "Usage: " << argv[0] << " surface3d.obj flattening.obj" << std::endl;
        return 1;
    }
    std::cerr << "Checking " << argv[1] << std::endl;

    Tetrahedra mrest, mres;
    read_by_extension(argv[1], mrest);
    read_by_extension(argv[2], mres);

    assert(mrest.ncells()==mres.ncells() && mrest.nverts()==mres.nverts());

    std::vector<double> X(mres.ncells());
    std::vector<double> Y(mres.ncells());

    for (int t : cell_iter(mrest)) {
        mat<3,3> ST = {{
            mrest.points[mrest.vert(t, 1)] - mrest.points[mrest.vert(t, 0)],
            mrest.points[mrest.vert(t, 2)] - mrest.points[mrest.vert(t, 0)],
            mrest.points[mrest.vert(t, 3)] - mrest.points[mrest.vert(t, 0)]
        }};
        mat<4,3> ref_tet = mat<4,3>{{ {-1,-1,-1},{1,0,0},{0,1,0},{0,0,1} }}*ST.invert_transpose();


        mat<3,3> J = {};

        for (int i : {0,1,2,3})
            for (int d : {0,1,2})
                J[d] += ref_tet[i]*mres.points[mres.vert(t,i)][d];

        mat3x3 G = J.transpose() * J;
        mat3x3 evec;
        vec3 eval;
        eigendecompose_symmetric(G, eval, evec);
        //        assert(eval.x>0 && eval.y>0);
        double smax = std::sqrt(eval.x);
        double smin = std::sqrt(eval.z);

        X[t] = J.det();
        Y[t] = smax/smin;

        std::cout << Y[t] << ", " << X[t] << std::endl;
    }

    int i1 = mres.ncells()/100;
    int i2 = mres.ncells()/20;

    std::sort(X.begin(), X.end());
    std::sort(Y.begin(), Y.end());

    std::cerr << "minscale100 " << X[0] << ", maxstretch100: " << Y[mres.ncells()-1] << std::endl;
    std::cerr << "minscale99 " << X[i1] << ", maxstretch99: " << Y[mres.ncells()-1-i1] << std::endl;
    std::cerr << "minscale95 " << X[i2] << ", maxstretch95: " << Y[mres.ncells()-1-i2] << std::endl;

//    std::cerr << "min(det J) = " << detmin << "; max(det J) = " << detmax << "; max(max ev / min ev) = " << evrat << std::endl;

    return 0;
}


