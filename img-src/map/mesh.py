import numpy as np

class Mesh():
    def __init__(self, nstacks=16, nslices=16):
        V, VT, T = [], [], []
        V.append([0., 0., 1.]) # top vertex
        for i in range(nstacks-1): # one vertex per stack / slice
            phi = np.pi*(i+1)/nstacks
            for j in range(nslices):
                theta = 2.*np.pi*j/nslices
                x = np.sin(phi)*np.cos(theta)
                y = np.sin(phi)*np.sin(theta)
                z = np.cos(phi)
                V.append([x, y, z])
        V.append([0., 0, -1.]) # bottom vertex

        for i in range(nslices): # top/bottom triangles
            T.append([ i+1, 0,(i+1)%nslices+1])
            T.append([(nstacks-1)*nslices+1, i+nslices*(nstacks-2)+1, (i+1)%nslices+nslices*(nstacks-2)+1])

        for j in range(nstacks-2):
            j0 = j*nslices+1
            j1 = (j+1)*nslices+1
            for i in range(nslices):
                i0 = j0+i
                i1 = j0+(i+1)%nslices
                i2 = j1+(i+1)%nslices
                i3 = j1+i
                T.append([i0, i1, i2])
                T.append([i0, i2, i3])

        for t in T:
            vt = []
            for v in t:
                x,y,z = V[v]
                phi = np.pi/2 - np.arccos(z) # latitude
                l = np.arctan2(y, x) # longitude
                if 0:
                    L = l/(2*np.pi)+.5
                    if len(vt)>0:
                        if vt[0][0]-L >  .5: L += 1
                        if vt[0][0]-L < -.5: L -= 1
                    vt.append([L, phi/np.pi+.5])  # equirectangular
                else:
                    cutoff = np.log(np.tan(85./180.*np.pi/2 + np.pi/4))
                    u = 0
                    if np.abs(phi)<85./180.*np.pi:
                        u = np.log(np.tan(np.pi/4. + phi/2))
                    L = l/(2*np.pi)+.5
                    if len(vt)>0:
                        if vt[0][0]-L >  .5: L += 1
                        if vt[0][0]-L < -.5: L -= 1
                    vt.append([L, u/(2*cutoff)+.5])  # mercator
            for v in vt:
                VT.append(v)
        self.V, self.VT, self.T = np.array(V), np.array(VT), np.array(T)
 
    @property
    def nverts(self):
        return len(self.V)

    @property
    def ntriangles(self):
        return len(self.T)

    @property
    def ncorners(self):
        return self.ntriangles*3;

    def org(self, c):
        return self.T[c//3][c%3]

    def dst(self, c):
        return self.T[c//3][(c+1)%3]

    def __str__(self):
        ret = ""
        for v in self.V:
            ret = ret + ("v %f %f %f\n" % (v[0], v[1], v[2]))
        for vt in self.VT:
            ret = ret + ("vt %f %f\n" % (vt[0], vt[1]))
        for f in range(self.ntriangles):
            t = self.T[f]
            ret = ret + ("f %d/%d %d/%d %d/%d\n" % (t[0]+1, f*3+1, t[1]+1, f*3+2, t[2]+1, f*3+3))
        return ret
