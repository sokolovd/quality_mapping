#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdint>
#undef NDEBUG
#include <cassert>
#define STB_IMAGE_IMPLEMENTATION
#include "../stb_image.h"

uint32_t pack_color(const uint8_t r, const uint8_t g, const uint8_t b, const uint8_t a=255) {
    return (a<<24) + (b<<16) + (g<<8) + r;
}

void unpack_color(const uint32_t &color, uint8_t &r, uint8_t &g, uint8_t &b, uint8_t &a) {
    r = (color >>  0) & 255;
    g = (color >>  8) & 255;
    b = (color >> 16) & 255;
    a = (color >> 24) & 255;
}

void drop_ppm_image(const std::string filename, const std::vector<uint32_t> &image, const size_t w, const size_t h) {
    assert(image.size() == w*h);
    std::ofstream ofs(filename);
    ofs << "P6\n" << w << " " << h << "\n255\n";
    for (size_t i = 0; i < h*w; ++i) {
        uint8_t r, g, b, a;
        unpack_color(image[i], r, g, b, a);
        ofs << static_cast<char>(r) << static_cast<char>(g) << static_cast<char>(b);
    }
    ofs.close();
}

bool load_texture(const std::string filename, std::vector<uint32_t> &texture, int &w, int &h) {
    int nchannels = -1;
    unsigned char *pixmap = stbi_load(filename.c_str(), &w, &h, &nchannels, 0);
    if (!pixmap) {
        std::cerr << "Error: can not load the textures" << std::endl;
        return false;
    }

    if (3!=nchannels) {
        std::cerr << "Error: the texture must be a 32 bit image" << std::endl;
        stbi_image_free(pixmap);
        return false;
    }

    texture = std::vector<uint32_t>(w*h);
    for (int j=0; j<h; j++) {
        for (int i=0; i<w; i++) {
            uint8_t r = pixmap[(i+j*w)*3+0];
            uint8_t g = pixmap[(i+j*w)*3+1];
            uint8_t b = pixmap[(i+j*w)*3+2];
            uint8_t a = 0;
            texture[i+j*w] = pack_color(r, g, b, a);
        }
    }
    stbi_image_free(pixmap);
    return true;
}

uint32_t lerp(const uint32_t &c0, const uint32_t &c1, double t) {
    uint8_t r0,g0,b0,a0,r1,g1,b1,a1,r,g,b;
    unpack_color(c0, r0,g0,b0,a0);
    unpack_color(c1, r1,g1,b1,a1);
    t = std::max(0., std::min(1., t));
    r = r0 + (r1-r0)*t;
    g = g0 + (g1-g0)*t;
    b = b0 + (b1-b0)*t;
    return pack_color(r,g,b);
}

uint32_t palette(double d) {
    const uint32_t red    = pack_color(255, 0, 0);
    const uint32_t green  = pack_color(0, 255, 0);
    const uint32_t blue   = pack_color(0, 0, 255);
    double x = std::max(0., std::min(1., d));
    if (x<.5)
        return lerp(blue, green, x*2.);
    else
        return lerp(green, red, x*2.-1.);
}


int main(int argc, char *argv[]) {
    const auto deg2rad = [](double degrees) {
        return degrees/180.*M_PI;
    };

    int merc_size;
    constexpr double merc_cutoff = std::log(std::tan(deg2rad(85.)/2. + M_PI/4.));
    std::vector<uint32_t> mercator_map;
    load_texture(argv[1], mercator_map, merc_size, merc_size);

    int eqw = 4424, eqh=2214;
    std::vector<uint32_t> equirectangular_map(eqw*eqh, 0);

    const auto mercator = [](double phi, double l) {
        return std::make_pair(std::log(std::tan(M_PI/4. + phi/2.)), l);
    };

    for (double i=0; i<merc_size; i+=.3) {
        for (double j=0; j<merc_size; j+=.3) {
            double u = (j/(double)merc_size-.5)*(2*merc_cutoff);
            double v = (i/(double)merc_size - .5)*M_PI*2;
            double phi = (std::atan(std::exp(u)) - M_PI/4.)*2.;
            size_t eq_y = (phi/M_PI+.5)*eqh ;
            size_t eq_x = (v/(M_PI*2)+.5)*eqw;
//          std::cerr << phi << " " << v << " " << eq_x << " " << eq_y << std::endl;
            equirectangular_map[eq_x + eq_y*eqw] = mercator_map[i+int(j)*merc_size];
        }
    }

    drop_ppm_image("equi.ppm", equirectangular_map, eqw, eqh);
    return 0;
}

