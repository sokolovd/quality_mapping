#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdint>
#undef NDEBUG
#include <cassert>
#define STB_IMAGE_IMPLEMENTATION
#include "../stb_image.h"

uint32_t pack_color(const uint8_t r, const uint8_t g, const uint8_t b, const uint8_t a=255) {
    return (a<<24) + (b<<16) + (g<<8) + r;
}

void unpack_color(const uint32_t &color, uint8_t &r, uint8_t &g, uint8_t &b, uint8_t &a) {
    r = (color >>  0) & 255;
    g = (color >>  8) & 255;
    b = (color >> 16) & 255;
    a = (color >> 24) & 255;
}

void drop_ppm_image(const std::string filename, const std::vector<uint32_t> &image, const size_t w, const size_t h) {
    assert(image.size() == w*h);
    std::ofstream ofs(filename);
    ofs << "P6\n" << w << " " << h << "\n255\n";
    for (size_t i = 0; i < h*w; ++i) {
        uint8_t r, g, b, a;
        unpack_color(image[i], r, g, b, a);
        ofs << static_cast<char>(r) << static_cast<char>(g) << static_cast<char>(b);
    }
    ofs.close();
}

bool load_texture(const std::string filename, std::vector<uint32_t> &texture, int &w, int &h) {
    int nchannels = -1;
    unsigned char *pixmap = stbi_load(filename.c_str(), &w, &h, &nchannels, 0);
    if (!pixmap) {
        std::cerr << "Error: can not load the textures" << std::endl;
        return false;
    }

    if (3!=nchannels) {
        std::cerr << "Error: the texture must be a 32 bit image" << std::endl;
        stbi_image_free(pixmap);
        return false;
    }

    texture = std::vector<uint32_t>(w*h);
    for (int j=0; j<h; j++) {
        for (int i=0; i<w; i++) {
            uint8_t r = pixmap[(i+j*w)*3+0];
            uint8_t g = pixmap[(i+j*w)*3+1];
            uint8_t b = pixmap[(i+j*w)*3+2];
            uint8_t a = 0;
            texture[i+j*w] = pack_color(r, g, b, a);
        }
    }
    stbi_image_free(pixmap);
    return true;
}

uint32_t lerp(const uint32_t &c0, const uint32_t &c1, double t) {
    uint8_t r0,g0,b0,a0,r1,g1,b1,a1,r,g,b;
    unpack_color(c0, r0,g0,b0,a0);
    unpack_color(c1, r1,g1,b1,a1);
    t = std::max(0., std::min(1., t));
    r = r0 + (r1-r0)*t;
    g = g0 + (g1-g0)*t;
    b = b0 + (b1-b0)*t;
    return pack_color(r,g,b);
}

uint32_t palette(double d) {
    const uint32_t red    = pack_color(255, 0, 0);
    const uint32_t green  = pack_color(0, 255, 0);
    const uint32_t blue   = pack_color(0, 0, 255);
    double x = std::max(0., std::min(1., d));
    if (x<.5)
        return lerp(blue, green, x*2.);
    else
        return lerp(green, red, x*2.-1.);
}


int main(int argc, char *argv[]) {
    const auto deg2rad = [](double degrees) {
        return degrees/180.*M_PI;
    };

    std::vector<uint32_t> equirectangular_map;
    int eqw, eqh;
    load_texture(argv[1], equirectangular_map, eqw, eqh);

    std::vector<uint32_t> grid(eqw*eqh, pack_color(255, 255, 255));
    std::vector<uint32_t> eqtranssib(eqw*eqh, pack_color(255, 255, 255));

//  constexpr double phi_from =  30./180.*M_PI;
//  constexpr double phi_to   =  80./180.*M_PI;
//  constexpr double l_from   =  20./180.*M_PI;
//  constexpr double l_to     = 190./180.*M_PI;

    constexpr double phi_from =  -90./180.*M_PI;
    constexpr double phi_to   =  90./180.*M_PI;
    constexpr double l_from   =  -180./180.*M_PI;
    constexpr double l_to     = 180./180.*M_PI;


    for (float phi=deg2rad(-80); phi<deg2rad(80); phi+=deg2rad(10)) {
        for (float l=l_from; l<l_to; l+=.001) {
            size_t eq_x = (l/(M_PI*2)+.5)*eqw;
            size_t eq_y = (phi/M_PI+.5)*eqh;
            grid[eq_x + (eqh-eq_y)*eqw] = pack_color(100, 100, 100);
        }
    }
    for (float phi=phi_from; phi<phi_to; phi+=.001) {
        for (float l=l_from; l<l_to; l+=deg2rad(10)) {
            size_t eq_x = (l/(M_PI*2)+.5)*eqw;
            size_t eq_y = (phi/M_PI+.5)*eqh;
            grid[eq_x + (eqh-eq_y)*eqw] = pack_color(100, 100, 100);
        }
    }

    {
    constexpr int merc_size = 2044;
    constexpr double merc_cutoff = std::log(std::tan(deg2rad(85.)/2. + M_PI/4.));
    int transsib[] = {1196,592,1196,592,1201,600,1201,600,1201,600,1206,606,1206,606,1206,606,1211,609,1211,609,1211,609,1216,615,1216,615,1216,615,1221,620,1221,620,1221,620,1224,624,1224,624,1224,624,1230,629,1230,629,1230,629,1233,633,1233,633,1233,633,1235,635,1235,635,1235,635,1238,635,1238,635,1238,635,1241,640,1241,640,1241,640,1246,644,1246,644,1246,644,1248,650,1248,650,1248,650,1249,654,1249,654,1249,654,1262,655,1262,655,1262,655,1267,656,1267,656,1267,656,1277,656,1277,656,1277,656,1283,658,1283,658,1283,658,1289,661,1289,661,1289,661,1298,663,1298,663,1298,663,1302,663,1302,663,1302,663,1309,661,1309,661,1309,661,1316,655,1316,655,1316,655,1323,654,1324,654,1324,654,1327,651,1327,651,1327,651,1333,647,1333,647,1333,647,1337,646,1338,646,1338,646,1348,642,1348,642,1348,642,1354,637,1354,637,1354,637,1362,637,1362,637,1362,637,1369,639,1369,639,1369,639,1378,638,1378,638,1378,638,1384,637,1384,637,1384,637,1390,635,1390,635,1390,635,1395,635,1396,635,1396,635,1401,638,1401,638,1402,638,1409,643,1409,643,1409,643,1414,645,1414,645,1415,645,1419,645,1420,645,1420,645,1438,645,1438,645,1438,645,1441,643,1441,643,1441,643,1445,640,1445,640,1445,640,1450,638,1450,638,1450,638,1453,637,1453,637,1453,637,1457,636,1457,636,1457,636,1462,635,1462,635,1462,635,1466,635,1466,635,1466,635,1470,637,1470,637,1470,637,1473,639,1473,639,1473,639,1477,641,1477,641,1477,641,1480,643,1480,643,1480,643,1483,643,1483,643,1483,643,1486,644,1486,643,1487,643,1489,643,1490,643,1490,643,1494,642,1494,642,1494,642,1497,641,1497,641,1497,641,1499,640,1499,640,1499,640,1503,637,1503,637,1503,637,1506,635,1506,635,1506,635,1508,633,1508,633,1508,633,1510,632,1510,632,1510,632,1513,631,1513,631,1513,631,1517,631,1517,631,1517,631,1519,633,1519,633,1519,633,1523,633,1523,633,1523,633,1528,633,1528,633,1528,633,1532,633,1532,633,1532,633,1537,633,1537,633,1537,633,1542,633,1542,633,1542,633,1545,633,1545,633,1545,633,1548,633,1548,633,1548,633,1550,635,1550,635,1550,635,1554,635,1554,635,1554,635,1560,635,1560,635,1560,635,1564,634,1564,634,1564,634,1567,631,1567,631,1567,631,1572,632,1572,632,1572,632,1574,633,1574,633,1574,633,1576,635,1576,635,1576,635,1579,637,1579,637,1579,637,1582,640,1582,640,1582,640,1583,643,1583,643,1583,643,1586,645,1586,645,1586,645,1589,648,1589,648,1589,648,1593,650,1593,650,1593,650,1597,653,1597,653,1597,653,1601,656,1601,656,1601,656,1604,659,1604,659,1604,659,1606,662,1606,662,1606,662,1610,666,1610,666,1610,666,1614,670,1614,670,1614,670,1616,673,1616,673,1616,673,1616,674,1616,674,1616,674,1614,675,1614,675,1614,675,1611,677,1611,677,1611,677,1611,679,1611,679,1611,679,1614,681,1614,681,1614,681,1616,681,1616,681,1616,681,1620,680,1620,680,1620,680,1624,679,1624,679,1624,679,1631,677,1631,677,1631,677,1636,676,1636,676,1636,676,1640,676,1640,676,1640,676,1642,675,1642,675,1642,675,1647,673,1647,673,1647,673,1650,672,1650,672,1650,672,1655,671,1655,671,1655,671,1660,672,1660,672,1660,672,1664,672,1664,672,1664,672,1670,673,1670,673,1670,673,1673,674,1673,674,1673,674,1676,674,1676,674,1676,674,1678,674,1678,674,1678,674,1680,674,1680,674,1680,674,1683,674,1683,674,1683,674,1686,673,1686,673,1686,673,1689,672,1689,672,1689,672,1691,670,1691,670,1691,670,1693,669,1693,669,1693,669,1695,666,1695,666,1695,666,1697,664,1697,664,1697,664,1699,663,1699,663,1699,663,1701,660,1701,660,1701,660,1702,659,1703,659,1703,659,1705,657,1705,657,1705,657,1708,656,1708,656,1708,656,1711,655,1711,655,1711,655,1714,654,1714,654,1714,654,1718,653,1718,653,1718,653,1721,653,1721,653,1721,653,1725,654,1725,654,1725,654,1728,655,1728,655,1728,655,1731,656,1731,656,1731,656,1735,659,1735,659,1735,659,1737,661,1737,661,1737,661,1739,664,1739,664,1739,664,1741,668,1741,668,1741,668,1743,672,1743,672,1743,672,1745,676,1745,676,1745,676,1745,679,1745,679,1745,679,1746,683,1746,683,1746,683,1749,688,1749,688,1749,688,1752,690,1752,690,1752,690,1757,692,1757,692,1757,692,1761,694,1761,694,1761,694,1764,694,1764,694,1764,694,1770,696,1770,696,1770,696,1772,696,1773,696,1773,696,1777,697,1777,697,1777,697,1782,700,1782,700,1782,700,1786,701,1786,701,1786,702,1791,702,1791,702,1791,702,1790,709,1790,709,1790,709,1790,712,1790,712,1790,712,1790,718,1789,718,1789,718,1788,722,1788,722,1788,722,1785,726,1785,726,1785,726,1783,733,1783,733,1783,733,1781,736,1781,736,1781,736,1779,738,1779,738,1779,738,1776,741,1776,741,1776,741,1774,743,1774,743,1774,743,1772,746,1772,746};
        int ntranssib = sizeof(transsib)/sizeof(int)/2;
        for (int i=0; i<ntranssib-1; i++) {
            int Ax = transsib[i*2+0];
            int Ay = transsib[i*2+1];
            int Bx = transsib[i*2+2];
            int By = transsib[i*2+3];
            for (double t=0; t<1; t+=0.001) {
                double Px = Ax*(1.-t) + Bx*t;
                double Py = Ay*(1.-t) + By*t;
                double v = (Px/merc_size-.5)*2*M_PI;
                double u = ((merc_size-1-Py)/merc_size-.5)*2*merc_cutoff;
                double phi = (std::atan(std::exp(u)) - M_PI/4.)*2.;
                size_t eq_x = (v/(M_PI*2)+.5)*eqw;
                size_t eq_y = (phi/M_PI+.5)*eqh;
//                std::cerr << eqw << " " << eqh<< " " << eq_x << " " << eq_y << " " << transsib.size() << " " << (eq_x + (eqh-eq_y)*eqw) << std::endl;
              eqtranssib[eq_x + (eqh-eq_y)*eqw] = 0;
            }
        }
    }


    const double A = 279.5/180.*M_PI;
    const double mu = (288.5 + 16.7/3600.)/180.*M_PI;
    const double r = 0.7800438;
    const double a = r*std::cos(mu)/std::cos(A);
    const double b = r*std::sin(mu)/std::sin(A);

    for (float t=0; t<M_PI*2; t+=.0001) {
        double x = a*std::cos(t);
        double y = b*std::sin(t);
        double up = x*std::cos(A) - y*std::sin(A);
        double vp = x*std::sin(A) + y*std::cos(A);
        double u =  up + 1.446177;
        double v = -vp + (105. + 45./60.)/180.*M_PI;
        double phi = (std::atan(std::exp(u)) - M_PI/4.)*2.;
                size_t eq_x = (v/(M_PI*2)+.5)*eqw;
                size_t eq_y = (phi/M_PI+.5)*eqh;
              eqtranssib[eq_x + (eqh-eq_y)*eqw] = 0;
     }

    drop_ppm_image("equigrid.ppm", grid, eqw, eqh);
    drop_ppm_image("transsib.ppm", eqtranssib, eqw, eqh);
    return 0;
}

