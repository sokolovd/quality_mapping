#include <iostream>

#include <ultimaille/all.h>

using namespace UM;

double triangle_area_2d(vec2 a, vec2 b, vec2 c) {
    return .5*((b.y-a.y)*(b.x+a.x) + (c.y-b.y)*(c.x+b.x) + (a.y-c.y)*(a.x+c.x));
}

int main(int argc, char** argv) {
    double scale = .05;
    Triangles mi;
    SurfaceAttributes attr = read_by_extension(argv[1], mi);

    PointAttribute<vec2> tex_coord("tex_coord", attr, mi);
    FacetAttribute<double> fi("f", attr, mi);
    SurfaceConnectivity fec(mi);

    Polygons mo;
    PointAttribute<double> fo(mo);
    int n = mi.nfacets();
    mo.points.create_points(n*3);
    mo.create_facets(n,3);

#if 1
    for (int t : facet_iter(mi)) {
        vec2 A,B,C;
        mi.util.project(t, A, B, C);
        mat<2,2> ST = {{B-A, C-A}};
        mat<3,2> ref_tri = mat<3,2>{{ {-1,-1},{1,0},{0,1} }}*ST.invert_transpose();

        mat<2,2> J0;
        for (int i : {0,1,2})
            for (int d : {0,1}) {
                int v = mi.vert(t,i);
                J0[d] += ref_tri[i]*tex_coord[v][d];
            }
        {
            double f0 = (J0[0]*J0[0] + J0[1]*J0[1])/(2.*J0.det());
            double g0 = (1+J0.det()*J0.det())/(2.*J0.det());
            fi[t] = .5*f0 + .5*g0;
        }
     }
#endif

/*
    { // scale the input geometry to have the same area as the target domain
        double target_area = 0, source_area = 0;
        for (int t : facet_iter(mi)) {
            vec2 a = tex_coord[mi.vert(t, 0)];
            vec2 b = tex_coord[mi.vert(t, 1)];
            vec2 c = tex_coord[mi.vert(t, 2)];
            double area = triangle_area_2d(a, b, c);
            um_assert(area>0);
            target_area += area;
            source_area += mi.util.unsigned_area(t);
        }

        um_assert(target_area>0); // ascertain mesh requirements
        for (int v : vert_iter(mi))
            tex_coord[v] *= std::sqrt(source_area/target_area);
    }
*/

    for (int v : vert_iter(mi)) {
        mi.points[v] = {tex_coord[v].x, tex_coord[v].y, fi[v]};
    }

    {
        vec3 bbmin, bbmax; // these are used to undo the scaling we apply to the model
        const double boxsize = 1.;
        BBox3 bb = mi.points.util.bbox();
        double maxside = std::max(bb.max.x-bb.min.x, bb.max.y-bb.min.y);
        for (vec3 &p : mi.points) {
            p = (p - (bb.max+bb.min)/2.)*boxsize/maxside + vec3(1,1,1)*boxsize/2;
        }
    }

    for (int i : range(n*3)) {
        mo.points[i] = mi.points[mi.vert(i/3, i%3)];
//      vec2 p = tex_coord[mi.vert(i/3, i%3)];
//      mo.points[i] = {p.x, p.y, fi[i/3]};
        mo.vert(i/3, i%3) = i;
    }


    for (int t : facet_iter(mi)) {
        for (int i : {0,1,2}) {
            mo.points[t*3+i].z = fi[t]*scale;
            fo[t*3+i] = fi[t];

            int oppc = fec.opposite(t*3+i);
            if (oppc<0 || fec.from(t*3+i) > fec.to(t*3+i)) continue;
            int oppf = fec.facet(oppc);

//          std::cerr << t << " " << i << std::endl;
            int offv = mo.points.create_points(4);
            int offf = mo.create_facets(1, 4);
            for (int i : range(4))
                mo.vert(offf, i) = offv+i;

            mo.points[offv+0] = mi.points[fec.from(t*3+i)];
            mo.points[offv+1] = mi.points[fec.to(t*3+i)];
            mo.points[offv+2] = mi.points[fec.from(oppc)];
            mo.points[offv+3] = mi.points[fec.to(oppc)];
            for (int i : range(2)) {
                mo.points[offv+i].z = fi[t]*scale;
                mo.points[offv+i+2].z = fi[oppf]*scale;
                fo[offv+i] = fi[t];
                fo[offv+i+2] = fi[oppf];
            }
        }
    }

#if 0
    { // colocate vertices
        std::vector<int> old2new;
        colocate(*mo.points.data, old2new, 1e-3);
        for (int f : facet_iter(mo))
            for (int lv : range(mo.facet_size(f)))
                mo.vert(f, lv) = old2new[mo.vert(f, lv)];
       std::cerr << mo.nverts() << std::endl;
        mo.delete_isolated_vertices();
       std::cerr << mo.nverts() << std::endl;
    }
#endif

    write_by_extension("result.geogram", mo, {{{"f", fo.ptr}}, {}, {}});

    return 0;
}

