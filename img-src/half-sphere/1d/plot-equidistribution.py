#!/usr/bin/python3

'''
import collections.abc

collections.MutableMapping = collections.abc.MutableMapping
collections.Mapping = collections.abc.Mapping
collections.MutableSequence = collections.abc.MutableSequence
collections.Sequence = collections.abc.Sequence
collections.Iterable = collections.abc.Iterable
collections.Iterator = collections.abc.Iterator
collections.MutableSet = collections.abc.MutableSet
collections.Callable = collections.abc.Callable
'''


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

plt.rcParams["font.family"] = "serif"
plt.rcParams["mathtext.fontset"] = "dejavuserif"
plt.rcParams['text.usetex'] = True
plt.rc('font', size=45)
plt.rcParams['hatch.color'] = 'white'
plt.rcParams['hatch.linewidth'] = 20.0
plt.rcParams['figure.autolayout'] = True
plt.rcParams["legend.framealpha"] = 1.
plt.rcParams["legend.frameon"] = False

fig, ax = plt.subplots(1, figsize=(16,9),dpi=100)

[t, r, f] = np.loadtxt("qis8k.csv", delimiter=',', skiprows=1, unpack=True)
[tsd, rsd, fsd] = np.loadtxt("sd8k.csv", delimiter=',', skiprows=1, unpack=True)
[tairy, rairy, fairy] = np.loadtxt("airy8k.csv", delimiter=',', skiprows=1, unpack=True)
[tarap, rarap, farap] = np.loadtxt("arap8k.csv", delimiter=',', skiprows=1, unpack=True)

for i in range(len(r)):
    r[i] = r[i] - np.sqrt(2./np.pi)*t[i]
    rsd[i] = rsd[i] - np.sqrt(2./np.pi)*t[i]
    rairy[i] = rairy[i] - np.sqrt(2./np.pi)*t[i]
    rarap[i] = rarap[i] - np.sqrt(2./np.pi)*t[i]

print(max(f), max(f)-min(f))
print(max(fsd), max(fsd)-min(fsd))
print(max(fairy), max(fairy)-min(fairy))

'''
print(min(f))
print(max(f))
'''

if 0:
    plt.xlabel("$\\theta$")
    plt.ylabel("distortion")
    plt.plot(t, f, label="$f=c$", linewidth=6, color = 'orange')
    plt.plot(t, fsd, label="$f_{SD}=c_{SD}$", linewidth=6, color='teal')
    plt.plot(t, fairy, label="$1+f^{ARAP}_{Airy}=c_{Airy}$", linewidth=6, color='red')
#    plt.plot(t, farap, label="$1+\sqrt{f^{ARAP}_{Airy}}$")
    plt.legend(loc="center", fancybox=True, framealpha=0.5)
    plt.tight_layout()
    plt.savefig("1d-max-f.png")
else:
    plt.xlabel("$\\theta$")
    plt.ylabel("$r(\\theta) - \sqrt{2/\pi}\cdot\\theta$")
    plt.plot(t, r, label="$f = c$", linewidth=6, color='orange')
    plt.plot(t, rsd, label="$f_{SD} = c_{SD}$", linewidth=6,linestyle=(0,(6, 6)), color='teal')
    plt.plot(t, rairy, label="$f^{ARAP}_{Airy} = c_{Airy}$",linewidth=6, color='red')
#    plt.plot(t, rarap, label="$\sqrt{f^{ARAP}_{Airy}(\dot r(\\theta), r(\\theta))} = \sqrt{c_3}$")
    plt.legend(bbox_to_anchor=(.4,.55))
    plt.tight_layout()
    plt.savefig("1d-max-r.png")
'''
#   plt.plot(t, a, label="$\min\limits_r \int_0^{\pi/2} f(\dot r(\\theta), r(\\theta))\sin\\theta d\\theta$", color="orange")
#   plt.plot(t, A, label="$\min\limits_r \int_0^{\pi/2} f_A(\dot r(\\theta), r(\\theta))\sin\\theta d\\theta$", color="red", linestyle='dashed')
#    plt.plot(t, A, label="Airy's flattening", color="red", linestyle='dashed')
'''



#plt.plot([t[0], t[-1]], [r[0], r[-1]], c='orange')
#plt.plot([t[0], t[-1]], [f[-1], f[-1]], c='red')
#plt.plot(t,r)#,marker='.')#c='blue')
#plt.plot(t,f,c='green')
#plt.plot(t,fsd,c='green')

#plt.plot(t, a, label="$\mathrm{argmin} \int_0^{\pi/2} f[\dot r, r](\\theta)\sin\\theta d\\theta$")
#plt.plot(t, q, label="$f[\dot r, r](\\theta) = c$")
#plt.plot(t, s)

#plt.show()


