#!/usr/bin/python3

'''
import collections.abc

collections.MutableMapping = collections.abc.MutableMapping
collections.Mapping = collections.abc.Mapping
collections.MutableSequence = collections.abc.MutableSequence
collections.Sequence = collections.abc.Sequence
collections.Iterable = collections.abc.Iterable
collections.Iterator = collections.abc.Iterator
collections.MutableSet = collections.abc.MutableSet
collections.Callable = collections.abc.Callable
'''


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

plt.rcParams["font.family"] = "serif"
plt.rcParams["mathtext.fontset"] = "dejavuserif"
plt.rcParams['text.usetex'] = True
plt.rc('font', size=45)
plt.rcParams['hatch.color'] = 'white'
plt.rcParams['hatch.linewidth'] = 20.0
plt.rcParams['figure.autolayout'] = True
plt.rcParams["legend.framealpha"] = 1.
plt.rcParams["legend.frameon"] = False

fig, ax = plt.subplots(1, figsize=(16,9),dpi=100)

[t, r, f] = np.loadtxt("avg8k.csv", delimiter=',', skiprows=1, unpack=True)
[tqis, rqis, fqis] = np.loadtxt("qis8k.csv", delimiter=',', skiprows=1, unpack=True)

for i in range(len(r)):
    r[i] = r[i] - np.sqrt(2./np.pi)*t[i]
    rqis[i] = rqis[i] - np.sqrt(2./np.pi)*t[i]

if 1:
    plt.xlabel("$\\theta$")
    plt.ylabel("$f$")
    plt.plot(t, f, label="$\min\limits_r \int_0^{\pi/2} f(\dot r(\\theta), r(\\theta))\\sin\\theta\ d\\theta$", linewidth=6,color='orange')
    plt.plot(t, fqis, label="$f(\dot r(\\theta), r(\\theta))=c$", linewidth=6, color='green')
    plt.legend(loc="upper left")
    plt.tight_layout()
    plt.savefig("1d-qis-f.png")
else:
    plt.xlabel("$\\theta$")
    plt.ylabel("$r(\\theta) - \sqrt{2/\pi}\cdot\\theta$")
    plt.plot(t, r, label="$\min\limits_r \int_0^{\pi/2} f(\dot r(\\theta), r(\\theta))\\sin\\theta\ d\\theta$", linewidth=6, color='orange')#, linestyle=(3,(6,6)) )
    plt.plot(t, rqis, label="$f(\dot r(\\theta), r(\\theta))=c$", linewidth=6, color='green')
    plt.legend(bbox_to_anchor=(.01,1.05),loc="upper left")
    plt.tight_layout()
    plt.savefig("1d-qis-r.png")
