#!/usr/bin/python3

'''
import collections.abc

collections.MutableMapping = collections.abc.MutableMapping
collections.Mapping = collections.abc.Mapping
collections.MutableSequence = collections.abc.MutableSequence
collections.Sequence = collections.abc.Sequence
collections.Iterable = collections.abc.Iterable
collections.Iterator = collections.abc.Iterator
collections.MutableSet = collections.abc.MutableSet
collections.Callable = collections.abc.Callable
'''


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

plt.rcParams["font.family"] = "serif"
plt.rcParams["mathtext.fontset"] = "dejavuserif"
plt.rcParams['text.usetex'] = True
plt.rc('font', size=45)
plt.rcParams['hatch.color'] = 'white'
plt.rcParams['hatch.linewidth'] = 20.0
plt.rcParams['figure.autolayout'] = True
plt.rcParams["legend.framealpha"] = 1.
plt.rcParams["legend.frameon"] = False

fig, ax = plt.subplots(1, figsize=(16,9),dpi=100)

[t, r, f] = np.loadtxt("avg8k.csv", delimiter=',', skiprows=1, unpack=True)
[tsd, rsd, fsd] = np.loadtxt("sd-avg8k.csv", delimiter=',', skiprows=1, unpack=True)
[tairy, rairy, fairy] = np.loadtxt("airy-avg8k.csv", delimiter=',', skiprows=1, unpack=True)
[tarap, rarap, farap] = np.loadtxt("arap-avg8k.csv", delimiter=',', skiprows=1, unpack=True)

A = []
for i in range(1,len(r)):
    dt = t[i]-t[i-1]
    drdt = (r[i]-r[i-1])/dt

    s1 = drdt
    s2 = r[i]/np.sin(t[i])
    print(s2/s1)

for i in range(len(r)):
    r[i] = r[i] - np.sqrt(2./np.pi)*t[i]
    rsd[i] = rsd[i] - np.sqrt(2./np.pi)*t[i]
    rairy[i] = rairy[i] - np.sqrt(2./np.pi)*t[i]
    rarap[i] = rarap[i] - np.sqrt(2./np.pi)*t[i]
    if (i>0):
        A.append(np.tan(t[i]/2) + 2/np.tan(t[i]/2)*np.log(1/np.cos(t[i]/2)) + np.sqrt((1-np.cos(t[i]))/(np.cos(t[i])+1))*(np.log(2)-1) -  np.sqrt(2./np.pi)*t[i])
    else:
        A.append(0)


'''
print(min(f))
print(max(f))
print(max(f)-min(f))
'''

if 1:
    plt.xlabel("$\\theta$")
    plt.ylabel("distortion")
    plt.plot(t, fairy, label="$1+f^{ARAP}_{Airy}$", linewidth=6,color='red')
    plt.plot(t, fsd, label="$f_{SD}$", linewidth=6, color='teal')
    plt.plot(t, f, label="$f$", linewidth=6,color='orange')
    plt.legend(loc="upper left", fancybox=True, framealpha=0.5)
    plt.tight_layout()
    plt.savefig("1d-average-f.png")
else:
    plt.xlabel("$\\theta$")
    plt.ylabel("$r(\\theta) - \sqrt{2/\pi}\cdot\\theta$")
    plt.plot(t, r, label="$\min\limits_r \int_0^{\pi/2} f\\sin\\theta\ d\\theta$", linewidth=6, color='orange')#, linestyle=(3,(6,6)) )
    plt.plot(t, rsd, label="$\min\limits_r \int_0^{\pi/2} f_{SD}\\sin\\theta\ d\\theta$", linewidth=6, linestyle=(0,(6,6)), color='teal')
    plt.plot(t, rairy, label="$\min\limits_r \int_0^{\pi/2}f^{ARAP}_{Airy}\\sin\\theta\ d\\theta$",linewidth=6,color='red')#,linestyle=(3,(6,6)))
#    plt.plot(t, A, label="Airy analytic",linewidth=4, linestyle=(-3,(6,6)))
#    plt.plot(t, rarap, label="$\min\limits_r \int_0^{\pi/2}\sqrt{f^{ARAP}_{Airy}}$",linewidth=4,linestyle='--', dashes=(5, 3))
#    plt.legend(loc="upper left")
    handles, labels = plt.gca().get_legend_handles_labels()
    #specify order of items in legend
    order = [2,1,0]
    #add legend to plot
    plt.legend([handles[idx] for idx in order],[labels[idx] for idx in order],bbox_to_anchor=(.0,1.05),loc="upper left")

    plt.tight_layout()
    plt.savefig("1d-average-r.png")
'''
#   plt.plot(t, a, label="$\min\limits_r \int_0^{\pi/2} f(\dot r(\\theta), r(\\theta))\sin\\theta d\\theta$", color="orange")
#   plt.plot(t, A, label="$\min\limits_r \int_0^{\pi/2} f_A(\dot r(\\theta), r(\\theta))\sin\\theta d\\theta$", color="red", linestyle='dashed')
#    plt.plot(t, A, label="Airy's flattening", color="red", linestyle='dashed')
'''



#plt.plot([t[0], t[-1]], [r[0], r[-1]], c='orange')
#plt.plot([t[0], t[-1]], [f[-1], f[-1]], c='red')
#plt.plot(t,r)#,marker='.')#c='blue')
#plt.plot(t,f,c='green')
#plt.plot(t,fsd,c='green')

#plt.plot(t, a, label="$\mathrm{argmin} \int_0^{\pi/2} f[\dot r, r](\\theta)\sin\\theta d\\theta$")
#plt.plot(t, q, label="$f[\dot r, r](\\theta) = c$")
#plt.plot(t, s)

#plt.show()


