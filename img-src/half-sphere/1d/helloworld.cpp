#include <iostream>
#include <cstring>
#include <fstream>
#include <sstream>

#include <iomanip>
#include <cmath>
#include "stlbfgs.h"

#pragma omp declare reduction(vec_double_plus : std::vector<double> : \
        std::transform(omp_out.begin(), omp_out.end(), omp_in.begin(), omp_out.begin(), std::plus<double>())) \
        initializer(omp_priv = std::vector<double>(omp_orig.size(), 0))

#define AVG
//#define SD
#define ARAP
//#define AIRY

int main() {
    std::cerr << std::setprecision(std::numeric_limits<double>::max_digits10);

    int n = 8192;


#if 1
    std::vector<double> r;
    std::ifstream in;
    in.open("../airy-avg8k.csv", std::ifstream::in);
    if (in.fail()) return -1;
    std::string line;
    bool flag = false;
    while (!in.eof()) {
        std::getline(in, line);
        if (!flag) {
            flag = true;
            continue;
        }
        if (!strlen(line.c_str())) break;
        std::istringstream iss(line.c_str());
        double a=0,b=0,c=0;
        char trash;
        iss>>a>>trash>>b>>trash>>c;
        r.push_back(b);
    }
//    int n = r.size();
#else
    std::vector<double> r(n);
    for (int i=0; i<n; i++) {
        double theta = i*M_PI_2/(n-1);
        r[i] = theta;
    }
#endif

    double h = M_PI_2/(n-1);

    const auto f0 = [&](const std::vector<double>& r, const int i)->double {
        double theta = i*h;

        double ri = r[i];
        double rj = r[i-1];
        double sint = sin(theta);

        double dr = (ri-rj)/h;
#ifdef SD
        return (dr*dr + pow(ri/sint,2) + 1./(dr*dr) + pow(sint/ri,2))/4;
#else
#ifdef ARAP
    double p = .99;
    return 1.+pow(pow(dr-1,2) + pow(ri/sint-1,2), p);
#else
#ifdef AIRY
    return 1.+pow(dr-1,2) + pow(ri/sint-1,2);
#else
        double s = .5; // dont forget it in the gradient
        return (1.-s)/2.*(dr*sint/ri + ri/(dr*sint)) + s/2.*(dr*ri/sint + sint/(dr*ri));
#endif
#endif
#endif
    };

    const auto qual_max = [&](const std::vector<double>& r)->double {
        double ret = 0;
        for (int i=1; i<n; i++) {
            ret = std::max(f0(r, i), ret);
        }
        return ret;
    };

        double mi = 1e10, ma=0;
        for (int i=1; i<n; i++) {
            ma = std::max(f0(r, i), ma);
            mi = std::min(f0(r, i), mi);
        }
        std::cerr << mi << "-" << ma << std::endl;

#ifdef AVG
    double t = 0;
#else
    double (1.-1e-10)/qual_max(r);
#endif

    const STLBFGS::func_grad_eval func = [&](const std::vector<double> &r, double &f, std::vector<double> &g) {
        f = 0;
        for (double &v : g) v = 0;

#pragma omp parallel for reduction(vec_double_plus:g) reduction(+:f)
        for (int i=1; i<n; i++) {
            if (i<n-1 && r[i]>=r[i+1]) {
                f = 1e20;
            }
            double theta = i*h;
            double sint = sin(theta);

            double ri = r[i];
            double rj = r[i-1];

            double f0_ = f0(r,i);
            if (1<t*f0_) f = 1e20;
            double ft = f0_/(1-t*f0_);

#ifdef SD
//          dr:(ri-rj)/h;
//          SD:(dr^2+ri^2/sin(theta)^2 + 1/dr^2 + sin(theta)^2/ri^2)/4;
// ARAP:sqrt((dr-1)^2 + (ri/sin(theta)-1)^2);
// AIRY:(dr-1)^2 + (ri/sin(theta)-1)^2;

            double gri = ((-(2*pow(sint,2))/pow(ri,3))+(2*ri)/pow(sint,2)+(2*(ri-rj))/(h*h)-(2*h*h)/pow(ri-rj,3))/4;
            double grj = ((2*h*h)/pow(ri-rj,3)-(2*(ri-rj))/(h*h))/4;
#else
#ifdef ARAP
    double p = .99;
            double gri = ((2*(ri/sint-1))/sint+(2*((ri-rj)/h-1))/h)*p*pow(f0_, p-1);
            double grj = -(2*((ri-rj)/h-1))/h*p*pow(f0_, p-1);
#else
#ifdef AIRY
            double gri = (2*(ri/sint-1))/sint+(2*((ri-rj)/h-1))/h;
            double grj = -(2*((ri-rj)/h-1))/h;
#else
            double s = .5;
            double gri = ((1-s)*((-((ri-rj)*sint)/(h*ri*ri))+sint/(h*ri)+h/((ri-rj)*sint)-(h*ri)/((ri-rj)*(ri-rj)*sint)))/2+(s*((-(h*sint)/(ri*ri*(ri-rj)))-(h*sint)/(ri*(ri-rj)*(ri-rj))+(ri-rj)/(h*sint)+ri/(h*sint)))/2;
            double grj = (s*((h*sint)/(ri*(ri-rj)*(ri-rj))-ri/(h*sint)))/2+((1-s)*((h*ri)/((ri-rj)*(ri-rj)*sint)-sint/(h*ri)))/2;
#endif
#endif
#endif

            f += ft*sint*h;
            g[i  ] += gri*sint*h/pow(1-t*f0_, 2);
            if (i>1)
            g[i-1] += grj*sint*h/pow(1-t*f0_, 2);
        }
    std::cerr << "f=" << f << std::endl;
    };

    for (int i=0; i<1000; i++) {
        double E_prev, E;
        std::vector<double> trash(n);
        func(r, E_prev, trash);
        double qual_max_prev = qual_max(r);


        STLBFGS::Optimizer opt{func};
//        opt.invH = { 10, false };
#ifdef AVG
        opt.ftol = 1e-9;
        opt.maxiter = 1e20; // AVG 1e6;
#else
        opt.ftol = 1e-14;
//        opt.gtol = 0;
        opt.maxiter = 1e6;
#endif
        opt.run(r);
        func(r, E, trash);
        std::cerr << "E: " << E_prev << " --> " << E << " t: " << t << " qual_max: " << qual_max(r) << std::endl;
        const double sigma = std::max(1.-E/E_prev, 1e-1);

        const double qmax = qual_max(r);
        if (std::abs(qual_max_prev - qmax)/qmax<1e-11) break;
        t = t + sigma*(1.-t*qmax)/qmax;
#ifdef AVG
        break;
#endif
    }


    std::cout << std::setprecision(std::numeric_limits<double>::max_digits10);

    std::cout << "#theta,r(theta),f(theta)" << std::endl;
    std::cout << "0," << r[0] << "," << f0(r,1) << std::endl;
    for (int i=1; i<n; i++) {
        double theta = i*h;
        std::cout << theta << ", " << r[i] << "," << f0(r,i) << std::endl;
    }

    std::cerr << "t = " << 1./qual_max(r) << std::endl;

    return 0;
}

