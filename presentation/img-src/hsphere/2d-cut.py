#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

plt.rcParams["font.family"] = "serif"
plt.rcParams["mathtext.fontset"] = "dejavuserif"
plt.rcParams['text.usetex'] = True
plt.rc('font', size=40)
plt.rcParams['hatch.color'] = 'white'
plt.rcParams['hatch.linewidth'] = 20.0
plt.rcParams['figure.autolayout'] = True
plt.rcParams["legend.framealpha"] = 1.

fig, ax = plt.subplots(1, figsize=(16,9),dpi=100)

[t, ft0, fsa, famips, fqis] = np.loadtxt("comparison.csv", delimiter=',', skiprows=1, unpack=True)

plt.xlabel("$\\theta$")
plt.ylabel("$f$")
#plt.plot(t, fsa, label="SA", ds="steps-mid", color="red", linewidth=4)
plt.plot(t, ft0, label="$\\min\\limits_{\\vec{x}(\\vec{\\xi})}\\int\\limits_\\Omega f(J) d\\xi$", ds="steps-mid", color="orange", linewidth=4)
#plt.plot(t, famips, label="AMIPS", ds="steps-mid", color="blue", linewidth=4)
plt.plot(t, fqis, label="$\\min\\limits_{\\vec{x}(\\vec{\\xi})}\\max\\limits_\\Omega f(J)$", ds="steps-mid", color="green", linewidth=4)
plt.legend(loc="upper left")
plt.tight_layout()
plt.savefig("2d-cut.png")



