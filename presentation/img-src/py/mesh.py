import numpy as np

class Mesh():
    def __init__(self,n=32):
        self.V,self.VT,self.T = [[0.,0.,0.]],[[0.,0.]],[]
        for i in range(n):
            a = i / n * 2. * np.pi
            self.V.append([np.cos(a), np.sin(a), 0.])
            self.VT.append([np.cos(a), np.sin(a)])
            self.T.append([0, i+1, (i+1)%n+1])
        self.VT[0] = [-0.008774540012600994, -0.0073465725630040028]
        self.VT[2] = [.7, -.04]

    @property
    def nverts(self):
        return len(self.V)

    @property
    def ntriangles(self):
        return len(self.T)

    def __str__(self):
        ret = ""
        for v in self.V:
            ret = ret + ("v %f %f %f\n" % (v[0], v[1], v[2]))
        for vt in self.VT:
            ret = ret + ("vt %f %f\n" % (vt[0], vt[1]))
        for t in self.T:
            ret = ret + ("f %d/%d %d/%d %d/%d\n" % (t[0]+1,t[0]+1, t[1]+1,t[1]+1, t[2]+1,t[2]+1))
        return ret
