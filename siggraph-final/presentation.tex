\documentclass[UKenglish,aspectratio=169,MathSerif]{beamer}
\usetheme[NoLogo]{pixel}


\usepackage[utf8]{inputenx} % For æ, ø, å
\usepackage{amsmath,amssymb,amsthm}

\usepackage[authoryear]{natbib}

\makeatletter
\def\beamerorig@set@color{%
  \pdfliteral{\current@color}%
  \aftergroup\reset@color
}
\def\beamerorig@reset@color{\pdfliteral{\current@color}}
\makeatother

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\proj}{proj}
\DeclareMathOperator{\Span}{span}
\DeclareMathOperator{\Div}{div}
%\usepackage{amsmath,amssymb,amsfonts}
\DeclareMathOperator{\vol}{vol}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\p}{\partial}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\R}{\mathbb R}
\newcommand{\eps}{\varepsilon}
\newcommand{\tfe}{\tilde{f}_{\eps}}
\newcommand{\tge}{\tilde{g}_{\eps}}


\definecolor{flotte}{rgb}{.282 .776 .773}
\definecolor{brickred}{rgb}{.79, 0.25, 0.32}
\definecolor{grass}{rgb}{.15, 0.60, 0.22}
\definecolor{postit}{rgb}{.35, 0.80, 0.99}
\newcommand{\highlight}[1]{\colorbox{cyan!30}{$\displaystyle#1$}}
\newcommand\unknown[1]{\textcolor{brickred}{#1}}
\newcommand\known[1]{\textcolor{grass}{#1}}
\newcommand\hidden[1]{\textcolor{lightgray}{#1}}


\usepackage[absolute,overlay]{textpos}

\usepackage{algpseudocode}
\renewcommand{\algorithmiccomment}[1]{{\hfill\color{gray} // \emph{#1}}}
\algnewcommand\algorithmicinput{\textbf{Input:}}
\algnewcommand\Input{\item[\algorithmicinput]}
\algnewcommand\algorithmicoutput{\textbf{Output:}}
\algnewcommand\Output{\item[\algorithmicoutput]}



\date{SIGGRAPH, July 31, 2024}

%\usepackage{enumitem}
\usepackage{xcolor}

\usepackage{tabularx,tikz}
\renewcommand{\tabularxcolumn}[1]{m{#1}}

\author{Dmitry Sokolov}
\title{In the Quest for Scale-optimal Mappings}
\subtitle{Vladimir Garanzha, Igor Kaporin, Liudmila Kudryavtseva, Fran{\c{c}}ois Protais, \underline{Dmitry Sokolov}}


\AtBeginSection[]
{
    \begin{frame}
        \frametitle{Table of Contents}
        \tableofcontents[currentsection]
    \end{frame}
}

\begin{document}

\begin{frame}{Chebyshev problem: best quasi-isometric mapping}
%\scriptsize

\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.17\paperwidth, yshift=-0.38\paperheight]
    {\includegraphics[height=0.5\paperheight]{img/Chebyshev.jpg}};

\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.6\paperwidth, yshift=-0.40\paperheight]
    {\includegraphics[width=0.60\paperwidth]{img/Chebyshev_USSR.jpg}};

\strut

\vspace{0.35\paperheight}

P.L. Chebyshev, 1856:
\begin{itemize}
\item Map of the country is optimal when it provides minimal relative path length error
\item Conformal mapping is optimal if isometric on the boundary of the domain% is the best conformal mapping% (proofs see  Grave 1911, Milnor 1969);
\item The relative length error of TransSib (St.Petersburg - Vladivostok railway) is less than 1.2\% (103 kilometers for about 9000 km total length).
\end{itemize}
\end{frame}

\begin{frame}{Quasi-isometric mapping: formal definition}
%\scriptsize
\tikz[remember picture,overlay]
  \node[opacity=1,inner sep=0pt] at (current page.north west)
  [xshift=0.27\paperwidth, yshift=-0.67\paperheight]
    {\includegraphics[height=0.58\paperheight]{img/notations.pdf}};

\vspace{-5mm}

Consider a map $\vec x(\vec \xi) : \Omega  \rightarrow \Omega_x $, where $\Omega, \Omega_x \subset \mathbb R^d$
with corresponding metric tensors $G_\xi(\vec\xi)$ and $G_x(\vec x)$.
For any simple curve $\gamma_\xi \in \Omega$ defined by 1D parameterization $\vec\xi(q), 0 \leq q \leq Q$,
and its image $\gamma_x$ we can measure their lengths as:

\vspace{-4mm}
$$
\qquad L_\xi = \int\limits_0^Q (\dot{\vec{\xi}}^\top G_\xi \dot{\vec{\xi}})^\frac12 \, d q
\qquad L_x = \int\limits_0^Q (\dot{\vec\xi}^\top J^\top G_x J\dot{\vec\xi})^\frac12 \, d q
$$

\vspace{-5mm}
\hspace{4.5cm} (recall that $\dot{\vec x} = J \dot{\vec \xi}$, where $J$ is the Jacobian matrix)

\vspace{3mm}

    \hspace{4.5cm}\colorbox{green!30}{A map $\vec x(\vec \xi)$ is said quasi-isometric if  there exists a bound $K$:}

    \hspace{8.32cm}  \colorbox{green!30}{$\quad\frac1{K}L_\xi \leq L_x \leq K L_\xi$ for any curve $\gamma_\xi$.}

\vspace{4mm}
\hspace{8.5cm}For a map regular enough we can\\
\hspace{8cm}reformulate it as a local matrix inequality:

\vspace{3mm}
\hspace{10cm}$\frac1{K^2} G_\xi \leq J^\top G_x J \leq K^2 G_\xi.$
\end{frame}

\begin{frame}{Elasticity as proxy problem}
\onslide<1>{
\tikz[remember picture,overlay]
\node[opacity=1,inner sep=0pt] at (current page.north west)
[xshift=0.5\paperwidth, yshift=-0.37\paperheight]
  {\includegraphics[width=0.9\paperwidth]{img/earth-map2.pdf}};
}

\onslide<2>{
\tikz[remember picture,overlay]
\node[opacity=1,inner sep=0pt] at (current page.north west)
[xshift=0.5\paperwidth, yshift=-0.37\paperheight]
  {\includegraphics[width=0.9\paperwidth]{img/dodecahedron-map2.pdf}};
}

	\vspace{35mm}
How to compute the best $\vec{x}(\vec{\xi})$? Matrix-based optimization seems hard...\\ \pause
\includegraphics[width=12pt]{img/idea.png} Solve a proxy problem instead:
mathematical elasticity minimizes the deviation from the isometric deformation state on average:

\begin{center}
\colorbox{green!30}{$\min\limits_{\vec{x}(\vec{\xi})}\int\limits_\Omega f(J)\, d\xi$, where $f$ is the distortion measure and $J$ is the Jacobian matrix.}
\end{center}
\end{frame}


\begin{frame}{G.B. Airy, 1861: Balance-of-Errors}
\scriptsize

\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.25\paperwidth, yshift=-0.35\paperheight]
    {\includegraphics[height=0.45\paperheight]{img/Airy.jpg}};

\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.25\paperwidth, yshift=-0.76\paperheight]
    {\includegraphics[width=0.45\paperwidth]{img/hsphere/notations.pdf}};

\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.70\paperwidth, yshift=-0.17\paperheight]
    {\includegraphics[width=0.45\paperwidth]{img/Airy_title.png}};

\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.70\paperwidth, yshift=-0.51\paperheight]
    {\includegraphics[width=0.45\paperwidth]{img/Airy_principle.png}}; 

\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.70\paperwidth, yshift=-0.87\paperheight]
    {\includegraphics[width=0.45\paperwidth]{img/Airy_functional.png}};


\end{frame}



\begin{frame}{Toy example: average vs max distortion}
%\vspace{30mm}
\only<1>{\centerline{\includegraphics[width=.9\linewidth]{img/question.pdf}}}\pause
\only<2>{\centerline{\includegraphics[width=.9\linewidth]{img/avg-max.png}}}
\end{frame}

\begin{frame}{Foldover-free maps: barrier functional}

\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.10\paperwidth, yshift=-0.35\paperheight]
    {\includegraphics[height=0.35\paperheight]{img/Ivanenko.jpg}};

\onslide<2>{
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=.73\paperwidth, yshift=-.52\paperheight]
    {\includegraphics[width=.55\linewidth]{img/ivanenko-crop.png}};
}
\onslide<3>{
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=.73\paperwidth, yshift=-.52\paperheight]
    {\includegraphics[width=.55\linewidth]{img/ivanenko-min-crop.png}};
}

\begin{textblock}{4.6}(3.0,2.32)
\begin{block}{Ivanenko, 1988}
    {\Large
%Inverse Dirichlet ($\Delta \vec{\xi}(\vec{x})=\vec{0}$):
    \vspace{.5mm}

$\min\int\limits_\Omega f(J) d\xi$\\
$  f(J) := \frac12\frac{\|J\|^2_F}{\max(0, \det J)}$
    }
    \vspace{.5mm}
\end{block}
\end{textblock}


\begin{textblock}{5.6}(0.5,10.)
    \onslide<3>
    {
    \Large
\includegraphics[height=16pt]{img/nolimits.png}~\raisebox{5pt}{\textbf{No inversions, but}}\\
\includegraphics[height=16pt]{img/slippery.png}~\raisebox{5pt}{\textbf{how to solve?}}
    }
\end{textblock}

\end{frame}

\begin{frame}{Untangling (Garanzha, 1999)}
%\vspace{30mm}
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=.25\paperwidth, yshift=-.64\paperheight]
    {\includegraphics[width=.45\linewidth]{img/ivanenko-min.png}};
\onslide<4>{
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=.75\paperwidth, yshift=-.65\paperheight]
    {\includegraphics[width=.43\linewidth]{img/untangling-1,0.png}};
}
\onslide<5>{
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=.75\paperwidth, yshift=-.65\paperheight]
    {\includegraphics[width=.43\linewidth]{img/untangling-0,1.png}};
}
\onslide<6>{
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=.75\paperwidth, yshift=-.65\paperheight]
    {\includegraphics[width=.43\linewidth]{img/untangling-0,01.png}};
}
\onslide<3->{
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=.90\paperwidth, yshift=-.24\paperheight]
    {\includegraphics[width=.20\linewidth]{img/chi-1.pdf}};
}

\begin{minipage}[!t]{.48\linewidth}\vspace{0pt}
\centering
\vspace{-5mm}
$\min\int\limits_\Omega f(J) d\xi, \quad  f(J) := \frac12\frac{\|J\|^2_F}{\textcolor{brickred}{\max(0, \det J)}}$

\vspace{1mm}
\includegraphics[height=16pt]{img/slippery.png}~\raisebox{5pt}{\textbf{How to solve?}}
\end{minipage}\hfill
\begin{minipage}[!t]{.48\linewidth}\vspace{0pt}
%\centering
\pause

\vspace{-5mm}
$\min\int\limits_\Omega f_{\textcolor{grass}{\varepsilon}}(J) d\xi, \quad  f_{\textcolor{grass}{\varepsilon}}(J) := \frac12\frac{\|J\|^2_F}{\textcolor{grass}{\chi_\varepsilon(\det J)}}$\\
\pause
$\chi_\varepsilon(D) := \frac{D+\sqrt{\varepsilon^2 + D^2}}{2}$
\pause

\pause

\end{minipage}
\end{frame}


%\iffalse
\begin{frame}{Bounded distortion: stiffening}
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=.75\paperwidth, yshift=-.23\paperheight]
    {\includegraphics[width=.35\linewidth]{img/avg-max-crop-bw-eqn.png}};
%\parbox{.5\textwidth}{
\begin{textblock}{6.6}(1.0,1.32)
\begin{block}{Garanzha, 2000}
$$\min\int\limits_\Omega w_t(J) \cdot f(J) d\xi$$
\vspace{-5mm}
\begin{align*}
f(J) := \frac12\frac{\|J\|^2_F}{\max(0, \det J)}\\
w_t(J) := \frac{1}{\max(0, 1 - t \cdot f(J))}
\end{align*}
\end{block}
%\pause
\vspace{-1mm}

\onslide<2-7>{
    \colorbox{green!30}{
        \parbox{.95\linewidth}{
            \begin{center}
                \vspace{-3mm}
                $f(J)\geq 1$, so if the integral is finite, \\
                the distortion is bounded: $\max\limits_\Omega f(J)<\frac1t$.
                \vspace{-3mm}
            \end{center}
        }}
    }


\onslide<7>{
%\vspace{3mm}
\includegraphics[height=16pt]{img/slippery.png}~\raisebox{5pt}{\textbf{How to choose $t$?}}\\
\includegraphics[height=16pt]{img/slippery.png}~\raisebox{5pt}{\textbf{How to solve?}}
%}
}
\end{textblock}
 \onslide<3>{
 \tikz[remember picture,overlay]
   \node[opacity=0.9,inner sep=0pt] at (current page.north west)
   [xshift=.75\paperwidth, yshift=-.64\paperheight]
     {\includegraphics[width=.45\linewidth]{img/stiffening-0,0.png}};
 }
 \onslide<4>{
 \tikz[remember picture,overlay]
   \node[opacity=0.9,inner sep=0pt] at (current page.north west)
   [xshift=.75\paperwidth, yshift=-.64\paperheight]
     {\includegraphics[width=.45\linewidth]{img/stiffening-0,1.png}};
 }
 \onslide<5>{
 \tikz[remember picture,overlay]
   \node[opacity=0.9,inner sep=0pt] at (current page.north west)
   [xshift=.75\paperwidth, yshift=-.64\paperheight]
     {\includegraphics[width=.45\linewidth]{img/stiffening-0,2.png}};
 }
 \onslide<6>{
 \tikz[remember picture,overlay]
   \node[opacity=0.9,inner sep=0pt] at (current page.north west)
   [xshift=.75\paperwidth, yshift=-.64\paperheight]
     {\includegraphics[width=.45\linewidth]{img/stiffening-0,3.png}};
 }
 \onslide<7>{
 \tikz[remember picture,overlay]
   \node[opacity=0.9,inner sep=0pt] at (current page.north west)
   [xshift=.75\paperwidth, yshift=-.64\paperheight]
     {\includegraphics[width=.45\linewidth]{img/stiffening-0,35.png}};
 }
 \tikz[remember picture,overlay]
   \node[opacity=0.9,inner sep=0pt] at (current page.north west)
   [xshift=.75\paperwidth, yshift=-.23\paperheight]
     {\includegraphics[width=.35\linewidth]{img/avg-max-crop-bw-eqn.png}};
\end{frame}



\begin{frame}{Foldover-free vs lowest distortion mapping}
\vspace{3mm}
\begin{minipage}[!t]{.45\linewidth}\vspace{0pt}
\raisebox{-1pt}{\includegraphics[width=12pt]{img/idea.png}}~Build a decreasing sequence of $\varepsilon^k$:
\begin{block}{Untangling algorithm}
\small
\begin{algorithmic}[1]
\Input $\vec{x}^{0}(\vec{\xi})$ \Comment{arbitrary initial guess} 
\Output $\vec{x}^*(\vec{\xi})$ \Comment{foldover-free map}
\State $k \leftarrow 0$;
\Repeat
\State compute $\varepsilon^k$; \raisebox{-5pt}{\includegraphics[height=16pt]{img/slippery.png}}
\State $\vec{x}^{k+1}(\vec{\xi}) \leftarrow \argmin\limits_{\vec{x}(\vec{\xi})}\int\limits_\Omega f_{\varepsilon^k} (J) d\xi$;
\State $k \leftarrow k+1$;
\Until{{\footnotesize $\min\limits_{\Omega} \det J>0$} \textbf{~and convergence}}
\State $\vec{x}^*(\vec{\xi}) \leftarrow \vec{x}^k(\vec{\xi})$;
\end{algorithmic}
\end{block}
\end{minipage}
\hspace{5mm}
\begin{minipage}[!t]{.48\linewidth}\vspace{0pt}
\pause
\raisebox{-1pt}{\includegraphics[width=12pt]{img/idea.png}}~Build an increasing sequence of $t^k$:
\begin{block}{Stiffening algorithm}
\small
\begin{algorithmic}[1]
\Input $\vec{x}^{0}(\vec{\xi})$ \Comment{foldover-free initial guess} 
\Output $\vec{x}^*(\vec{\xi})$ \Comment{lowest-distortion map}
\State $k \leftarrow 0$;
\Repeat
\State compute $t^k$;  \raisebox{-5pt}{\includegraphics[height=16pt]{img/slippery.png}}
\State ${\vec{x}^{k+1}(\vec{\xi}) \leftarrow \argmin\limits_{\vec{x}(\vec{\xi})}\int\limits_\Omega \frac{f (J)}{\max(0, 1-t^k\cdot f(J))} d\xi}$;
\State $k \leftarrow k+1$;
\Until{\textbf{convergence~}}
\vspace{1.5mm}
\State $\vec{x}^*(\vec{\xi}) \leftarrow \vec{x}^k(\vec{\xi})$;
\end{algorithmic}
\end{block}
\end{minipage}
\end{frame}


\begin{frame}{``Finite number of steps'' theorems}
\vspace{2mm}
\begin{minipage}[!t]{.45\linewidth}\vspace{0pt}
\raisebox{-1pt}{\includegraphics[width=12pt]{img/idea.png}}~Build a decreasing sequence of $\varepsilon^k$:
[Garanzha et al 2021]\\
Foldover-Free Maps in 50 Lines of Code
\begin{block}{\small Untangling in a finite number of steps}
If we have an efficient solver$^*$ and
the admissible set is not empty, \\

then it is reachable by solving a finite number of minimization problems.

\vspace{10mm}
\textit{\small $^*$ Few counter-examples were found.}
\end{block}
\end{minipage}
\hspace{5mm}
\begin{minipage}[!t]{.48\linewidth}\vspace{0pt}
\pause

\raisebox{-1pt}{\includegraphics[width=12pt]{img/idea.png}}~Build an increasing sequence of $t^k$:
[Garanzha et al 2024]\\
In the Quest for Scale-Optimal Mappings
\begin{block}{\small Bounded distortion in a finite number of steps}
If we have an efficient solver$^{**}$ and
the admissible set is not empty for a given distortion bound,\\
then it is reachable by solving a finite number of minimization problems.

\vspace{10mm}
\textit{\small $^{**}$ Counter-examples are not (yet?) exposed.}
\end{block}
\end{minipage}
\end{frame}


\begin{frame}{Free-boundary flattening example}
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.5\paperwidth, yshift=-0.47\paperheight]
    {\includegraphics[width=.8\paperwidth]{img/teaser.jpg}};


 \setlength{\TPHorizModule}{\textwidth}
 \setlength{\TPVertModule}{\textheight}

\begin{textblock}{0.5}(0.4,0.92)
$\min\limits_{\vec{x}(\vec{\xi})}\int\limits_\Omega f(J) d\xi$
\end{textblock}

\begin{textblock}{0.5}(0.75,0.92)
$\min\limits_{\vec{x}(\vec{\xi})}\max\limits_\Omega f(J)$
\end{textblock}
\end{frame}

\iffalse
\begin{frame}{Free-boundary flattening stress test}
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.5\paperwidth, yshift=-0.49\paperheight]
    {\includegraphics[width=.8\paperwidth]{img/stress-test.jpg}};


 \setlength{\TPHorizModule}{\textwidth}
 \setlength{\TPVertModule}{\textheight}

\begin{textblock}{0.5}(0.17,0.92)
$\min\limits_{\vec{x}(\vec{\xi})}\int\limits_\Omega f(J) d\xi$
\end{textblock}

\begin{textblock}{0.5}(0.35,0.92)
$\min\limits_{\vec{x}(\vec{\xi})}\max\limits_\Omega f(J)$
\end{textblock}

\begin{textblock}{0.5}(0.62,0.92)
$\min\limits_{\vec{x}(\vec{\xi})}\int\limits_\Omega f(J) d\xi$
\end{textblock}

\begin{textblock}{0.5}(0.8,0.92)
$\min\limits_{\vec{x}(\vec{\xi})}\max\limits_\Omega f(J)$
\end{textblock}
\end{frame}
\fi

\begin{frame}{3D deformation example}
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.5\paperwidth, yshift=-0.47\paperheight]
    {\includegraphics[width=\paperwidth]{img/wrench.jpg}};
 \setlength{\TPHorizModule}{\textwidth}
 \setlength{\TPVertModule}{\textheight}
\begin{textblock}{0.5}(0.23,0.92)
$\min\limits_{\vec{x}(\vec{\xi})}\int\limits_\Omega f(J) d\xi$
\end{textblock}

\begin{textblock}{0.5}(0.75,0.92)
$\min\limits_{\vec{x}(\vec{\xi})}\max\limits_\Omega f(J)$
\end{textblock}
\end{frame}

\iffalse
\begin{frame}{Flat Earth mathematics}
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.5\paperwidth, yshift=-0.3\paperheight]
    {\includegraphics[width=.7\paperwidth]{img/hsphere/notations.pdf}};
 \setlength{\TPHorizModule}{\textwidth}
 \setlength{\TPVertModule}{\textheight}

	
\begin{textblock}{0.57}(0.05,0.52)
	\begin{block}{Ground truth}
		Azimuthal equidistant projection is scale-optimal:
		$$r(\theta) = \sqrt{\frac{2}{\pi}} \theta$$
		(see [Milnor 1969] for the proof)
%$\min\limits_{\vec{x}(\vec{\xi})}\int\limits_\Omega f(J) d\xi$
	\end{block}
\end{textblock}

\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.8\paperwidth, yshift=-0.7\paperheight]
    {\includegraphics[width=.3\paperwidth]{img/hsphere/un-logo.pdf}};
\end{frame}

\begin{frame}{Flat Earth mathematics}
\onslide<1>{
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.5\paperwidth, yshift=-0.3\paperheight]
    {\includegraphics[width=.7\paperwidth]{img/hsphere/notations.pdf}};
}

\onslide<1->{
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.25\paperwidth, yshift=-0.72\paperheight]
    {\includegraphics[width=.45\paperwidth]{img/hsphere/1d-qis-r.png}};

\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.72\paperwidth, yshift=-0.72\paperheight]
    {\includegraphics[width=.45\paperwidth]{img/hsphere/1d-qis-f.png}};
}
\onslide<2>{
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.27\paperwidth, yshift=-0.3\paperheight]
    {\includegraphics[width=.45\paperwidth]{img/hsphere/comp.pdf}};
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.725\paperwidth, yshift=-0.28\paperheight]
    {\includegraphics[width=.445\paperwidth]{img/hsphere/2d-cut.png}};
}\end{frame}
\fi

\begin{frame}{Massive testing}
\onslide<1>{
\tikz[remember picture,overlay]
  \node[opacity=0.9,inner sep=0pt] at (current page.north west)
  [xshift=0.5\paperwidth, yshift=-0.53\paperheight]
    {\includegraphics[width=.7\paperwidth]{img/db.png}};
}

\onslide<2>{
\tikz[remember picture,overlay]
  \node[opacity=0.5,inner sep=0pt] at (current page.north west)
  [xshift=0.5\paperwidth, yshift=-0.53\paperheight]
    {\includegraphics[width=.7\paperwidth]{img/db.png}};

\vspace{13mm}
\centerline{{\fontsize{64}{64} \selectfont \textbf{It works.}}}
	}
\end{frame}

\end{document}

