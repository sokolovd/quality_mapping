import numpy as np
import matplotlib.pyplot as plt
import sys
from matplotlib import collections  as mc


plt.rcParams["font.family"] = "serif"
plt.rcParams["mathtext.fontset"] = "dejavuserif"
plt.rcParams['text.usetex'] = True
plt.rc('font', size=24)
plt.rcParams['hatch.color'] = 'white'
#plt.rcParams['hatch.linewidth'] = 20.0
plt.rcParams['figure.autolayout'] = True
plt.rcParams["legend.framealpha"] = 1.

fig, ax = plt.subplots(1, figsize=(10.80/2,10.80/2),dpi=200)

[Xf, Yf, Xw, Yw] = np.loadtxt("2d.csv", delimiter=',', unpack=True)

#fig = plt.figure()
ax = plt.gca()
lines = zip(zip(Xf, Yf), zip(Xw, Yw))

#c = np.array([(1, 0, 0, 1), (0, 1, 0, 1), (0, 0, 1, 1)])
#lc = mc.LineCollection(lines, colors=c, linewidths=0.1)
lc = mc.LineCollection(lines, color='gray', linewidths=0.2)
ax.add_collection(lc)

ax.plot(Xf, Yf, 'o', c='red', markeredgecolor='none', markersize=2, alpha=.5)
ax.plot(Xw, Yw, 'o', c='green', markeredgecolor='none', markersize=2, alpha=.5)





'''
for i in range(len(Xf)):
    Xw[i] -= Xf[i]
    Yw[i] -= Yf[i]
ax.quiver(Xf, Yf, Xw, Yw)
'''


ax.set_yscale('log')
ax.set_xscale('log')

#ax.set_title("top 95\% quality")
ax.set_title("2D benchmark")

ax.set_ylabel('$\max \sigma_1(J)/\sigma_2(J)$')
ax.set_xlabel('$\min \mathrm{det} J$')
ax.set_xlim(right=1.)

ax.set_xticks([10**(-14), 10**(-9), 10**(-4), 10**(0)])
#ax.set_xticks([10**(-20), 10**(-13), 10**(-7), 10**(-1)])

#ax.set_xticks([10**(-5), 10**(-3), 10**(-1)])
#ax.set_xticks([10**(-2), 10**(-1), 10**(-0)])
ax.set_yticks([10**(6), 10**(4), 10**(2), 10**(0)])

plt.tight_layout()
plt.savefig("2d-benchmark-scatter.png")

#ax.legend()
#plt.show()


