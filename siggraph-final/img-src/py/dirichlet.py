from mesh import Mesh
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import collections  as mc


plt.rcParams["font.family"] = "serif"
plt.rcParams["mathtext.fontset"] = "dejavuserif"
plt.rcParams['text.usetex'] = True
plt.rc('font', size=24)
plt.rcParams['hatch.color'] = 'white'
plt.rcParams['hatch.linewidth'] = 20.0
plt.rcParams['figure.autolayout'] = True
plt.rcParams["legend.framealpha"] = 1.

fig, ax = plt.subplots(1, figsize=(10.80/2,10.80/2),dpi=200)


m = Mesh()

def triangle_area_2d(a, b, c):
    return .5*((b[1]-a[1])*(b[0]+a[0]) + (c[1]-b[1])*(c[0]+b[0]) + (a[1]-c[1])*(a[0]+c[0]))

def jacobian(t):
    tri = m.T[t]
    R = np.matrix([[-1.,-1.],[1.,0.],[0.,1.]]) * np.matrix([[m.V[tri[1]][0] - m.V[tri[0]][0], m.V[tri[1]][1] - m.V[tri[0]][1] ], [m.V[tri[2]][0] - m.V[tri[0]][0], m.V[tri[2]][1] - m.V[tri[0]][1]]]).I.T
    J = np.matrix([[0.,0.],[0.,0.]])
    for i in range(3):
        for d in range(2):
            for j in range(2):
                J[d,j] = J[d,j] + R[i,j]*m.VT[tri[i]][d]
    return J

def chi(eps, det):
#    return .5*eps*eps / (np.sqrt(eps*eps + det*det) - det)
    return (det + np.sqrt(eps*eps + det*det))*.5

def energy(u,v):
    old = m.VT[0]
    m.VT[0] = [u,v]

    area = triangle_area_2d([m.V[0][0], m.V[0][1]], [m.V[1][0], m.V[1][1]], [m.V[2][0], m.V[2][1]])

    F = 0
    for t in range(m.ntriangles):
        J = jacobian(t)
        det = np.linalg.det(J)
#        t = 0#.35
#        f = (np.linalg.norm(J,ord='fro')**2)/(2.*det)
#        if det<0:
#            F = np.inf
#            break
#        F = F + f*area

#        if det<0 or t*f>1.:
#            F = np.inf
#            break
#        F = F + f/(1.-t*f)*area

#        F = F+(np.linalg.norm(J,ord='fro')**2)/(2.*chi(1e-1,det)) * area
        F = F+(np.linalg.norm(J,ord='fro')**2)/(2.) * area

    m.VT[0] = old
    return F


res = 256
xlist = np.linspace(-1.2, 1.2, res)
ylist = np.linspace(-1.2, 1.2, res)
X, Y = np.meshgrid(xlist, ylist)
Z = np.zeros((res, res))
for i in range(res):
    for j in range(res):
        Z[i,j] = energy(X[0,j], Y[i,0])


#m.VT[0] = [-0.0051973462361073457,-0.37609155154911106] # untangling
#m.VT[0] = [-0.36285102751053927, -0.71394365430171391] # stiffening
wireframe = [[],[]]
for tri in m.T:
    wireframe[0].append([m.VT[tri[0]][0], m.VT[tri[0]][1]])
    wireframe[1].append([m.VT[tri[1]][0], m.VT[tri[1]][1]])
    wireframe[0].append([m.VT[tri[1]][0], m.VT[tri[1]][1]])
    wireframe[1].append([m.VT[tri[2]][0], m.VT[tri[2]][1]])
    wireframe[0].append([m.VT[tri[2]][0], m.VT[tri[2]][1]])
    wireframe[1].append([m.VT[tri[0]][0], m.VT[tri[0]][1]])

lc = mc.LineCollection(wireframe, color='black', linewidths=.6)
#ax.add_collection(lc)



from matplotlib import cm, ticker

#ax = plt.contourf(X,Y,Z, 1000, locator=ticker.LogLocator(), cmap='RdYlGn_r')
#ax = plt.contourf(X,Y,Z, 1000, vmax=10,cmap='RdYlGn_r')

#im = ax.imshow(Z, interpolation='nearest', cmap=cm.RdYlGn_r, origin='lower', extent=[-1.2, 1.2, -1.2, 1.2], vmax=10, vmin=Z.min())
from matplotlib.colors import LogNorm

im = ax.imshow(Z, interpolation='gaussian',  cmap=cm.RdYlGn_r, origin='lower', extent=[-1.2, 1.2, -1.2, 1.2])
#im = ax.imshow(Z, interpolation='nearest',  cmap=cm.RdYlGn_r, origin='lower', extent=[-1.2, 1.2, -1.2, 1.2], norm=LogNorm())
#im = ax.imshow(Z, interpolation='gaussian',  cmap=cm.RdYlGn_r, origin='lower', extent=[-1.2, 1.2, -1.2, 1.2])
#plt.contour(Z, [10**1,10**2,10**3,10**4,10**5,10**6],origin='lower', extent=[-1.2, 1.2, -1.2, 1.2], colors='k')

ax.axes.xaxis.set_visible(False)
ax.axes.yaxis.set_visible(False)
plt.colorbar(im ,fraction=0.046, pad=0.04)

#plt.tight_layout()

plt.savefig("dirichlet.png")

plt.show()

#print(m) # output smoothed mesh
