#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#undef NDEBUG
#include <cassert>
#include <ultimaille/all.h>

using namespace UM;

static void read_lock(const std::string& filename, PointAttribute<bool>& locks) {
    std::ifstream in;
    in.open(filename, std::ifstream::in);
    if (in.fail()) {
        std::cerr << "Failed to open " << filename << std::endl;
        return;
    }
    std::string line;
    std::getline(in, line);
    while (!in.eof()) {
        std::getline(in, line);
        if (line == "") continue;
        std::istringstream iss(line.c_str());
        int v;
        iss >> v;
        locks[v] = true;
    }
}


int main(int argc, char** argv) {
    constexpr double theta = 1./2.; // the energy is (1-theta)*(shape energy) + theta*(area energy)

    if (argc<3) {
        std::cerr << "args" << std::endl;
        return 1;
    }
    std::cerr << "Checking " << argv[1] << std::endl;


    Tetrahedra ref, opt;
    read_by_extension(argv[1], ref);
    read_by_extension(argv[2], opt);

    PointAttribute<bool> lock(opt);
    CellAttribute<double> F(opt);
    CellAttribute<double> D(opt);
    CellAttribute<double> T(opt);

    read_lock("../lock.txt", lock);

    assert(ref.ncells()==opt.ncells());

    vec2 P = {std::numeric_limits<double>::max(), -std::numeric_limits<double>::max()};

    double distortion_max = 0;
    for (int t : cell_iter(ref)) {
        mat<3,3> ST = {{
            ref.points[ref.vert(t, 1)] - ref.points[ref.vert(t, 0)],
            ref.points[ref.vert(t, 2)] - ref.points[ref.vert(t, 0)],
            ref.points[ref.vert(t, 3)] - ref.points[ref.vert(t, 0)]
        }};
        mat<4,3> ref_tet = mat<4,3>{{ {-1,-1,-1},{1,0,0},{0,1,0},{0,0,1} }}*ST.invert_transpose();

        mat<3,3> J;
        for (int i : {0,1,2,3})
            for (int d : {0,1,2}) {
                assert(ref.vert(t,i)==opt.vert(t,i));
                int v = ref.vert(t,i);
                J[d] += ref_tet[i]*opt.points[v][d];
            }


        double det = J.det();
        double f = J.sumsqr()/(3.*pow(det, 2./3.));
        double g = (1+det*det)/(2.*det);
        distortion_max = std::max(distortion_max, (1.-theta)*f + theta*g);
        F[t] = (1.-theta)*f + theta*g;

        mat3x3 G = J.transpose() * J;
        mat3x3 evec;
        vec3 eval;
        eigendecompose_symmetric(G, eval, evec);

        double x = J.det();
        double y = std::sqrt(eval.x/eval.z);
        P.x = std::min(x, P.x);
        P.y = std::max(y, P.y);

      std::cerr << F[t] << std::endl;
//      std::cerr << y << "," << x << std::endl;

        D[t] = x;
        T[t] = y;
    }

//    std::cout << distortion_max<< "," <<P << std::endl;
//  write_geogram("attr.geogram", opt, {{{"selection", lock.ptr}}, {{"f", F.ptr}, {"det", D.ptr}, {"cond", T.ptr}}, {}, {}});


    return 0;
}

