#!/bin/bash
rm std.txt
rm err.txt
for i in `find /home/ssloy/nonnos/db/ -name  "init.vtk"|shuf`; do 
REFFILE=`dirname $i`/rest.vtk
INIFILE=`dirname $i`/untangled.geogram
RESFILE=`dirname $i`/optimized.geogram
if [ -f "$RESFILE" ]; then
    echo "$RESFILE exists."
else 
	./untangle3d $INIFILE $REFFILE $RESFILE >>std.txt 2>>err.txt;
fi

done

