#!/bin/bash

for i in `find /mnt/Locally-Injective/ -name  "input.obj"|sort`; do
echo -n "`dirname $i` "
./db-qual2d `dirname $i`/t0-free-boundary.obj
./db-qual2d `dirname $i`/t-free-boundary.obj
./db-qual2d `dirname $i`/simplex-assembly.obj
./db-qual2d `dirname $i`/lbd.obj
./db-qual2d `dirname $i`/amips.obj
./db-qual2d `dirname $i`/idp.obj
echo
done

