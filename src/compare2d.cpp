#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#undef NDEBUG
#include <cassert>
#include <ultimaille/all.h>

using namespace UM;
double triangle_area_2d(vec2 a, vec2 b, vec2 c) {
    return .5*((b.y-a.y)*(b.x+a.x) + (c.y-b.y)*(c.x+b.x) + (a.y-c.y)*(a.x+c.x));
}


int main(int argc, char** argv) {
    if (2>argc) {
        std::cerr << "Usage: " << argv[0] << " path/" << std::endl;
        return 1;
    }
    std::cerr << "Checking " << argv[1] << std::endl;

    Triangles m0, m1;
    SurfaceAttributes a0 = read_by_extension(argv[1]+std::string("/untangled.geogram"), m0);
    SurfaceAttributes a1 = read_by_extension(argv[1]+std::string("/optimized.geogram"), m1);
    PointAttribute<vec2> tex0("tex_coord", a0, m0);
    PointAttribute<vec2> tex1("tex_coord", a1, m1);

    assert(m0.nfacets()==m1.nfacets() && m0.nverts()==m1.nverts());

    const double theta = 0.5;
    double qual_max0 =  -std::numeric_limits<double>::max();

    double qual_max1 =  -std::numeric_limits<double>::max();

    vec2 P0 = {std::numeric_limits<double>::max(), -std::numeric_limits<double>::max()};
    vec2 P1 = {std::numeric_limits<double>::max(), -std::numeric_limits<double>::max()};

#if 0
    double target_area = 0;
    for (int t : facet_iter(m0)) {
        vec2 a = tex0[m0.vert(t, 0)];
        vec2 b = tex0[m0.vert(t, 1)];
        vec2 c = tex0[m0.vert(t, 2)];
        target_area += triangle_area_2d(a, b, c);
    }
    double source_area = 0;
    for (int t : facet_iter(m0))
        source_area += m0.util.unsigned_area(t);

    for (vec2 &p : tex0.ptr->data)
        p /= std::sqrt(target_area/source_area);
    for (vec2 &p : tex1.ptr->data)
        p /= std::sqrt(target_area/source_area);
#endif


    for (int t : facet_iter(m0)) {
        vec2 A,B,C;
        m0.util.project(t, A, B, C);
        mat<2,2> ST = {{B-A, C-A}};
        mat<3,2> ref_tri = mat<3,2>{{ {-1,-1},{1,0},{0,1} }}*ST.invert_transpose();

        mat<2,2> J0, J1;
        for (int i : {0,1,2})
            for (int d : {0,1}) {
                assert(m0.vert(t,i)==m1.vert(t,i));
                int v = m0.vert(t,i);
                J0[d] += ref_tri[i]*tex0[v][d];
                J1[d] += ref_tri[i]*tex1[v][d];
            }

        {
            double f0 = (J0[0]*J0[0] + J0[1]*J0[1])/(2.*J0.det());
            double g0 = (1+J0.det()*J0.det())/(2.*J0.det());
            qual_max0 = std::max(qual_max0, ((1.-theta)*f0 + theta*g0));
        }

        {
            double f1 = (J1[0]*J1[0] + J1[1]*J1[1])/(2.*J1.det());
            double g1 = (1+J1.det()*J1.det())/(2.*J1.det());
            qual_max1 = std::max(qual_max1, ((1.-theta)*f1 + theta*g1));
        }


        mat2x2 G0 = J0.transpose() * J0;
        mat2x2 G1 = J1.transpose() * J1;
        mat2x2 evec0, evec1;
        vec2 eval0, eval1;
        eigendecompose_symmetric(G0, eval0, evec0);
        eigendecompose_symmetric(G1, eval1, evec1);

        P0.x = std::min(J0.det(), P0.x);
        P0.y = std::max(std::sqrt(eval0.x/eval0.y), P0.y);
        P1.x = std::min(J1.det(), P1.x);
        P1.y = std::max(std::sqrt(eval1.x/eval1.y), P1.y);
    }

    std::cout << P0 << " " << P1 << std::endl;
    //    std::cerr << qual_max0 << " " << qual_max1 << std::endl;


    return 0;
}

