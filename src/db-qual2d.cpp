#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#undef NDEBUG
#include <cassert>
#include <ultimaille/all.h>

using namespace UM;
double triangle_area_2d(vec2 a, vec2 b, vec2 c) {
    return .5*((b.y-a.y)*(b.x+a.x) + (c.y-b.y)*(c.x+b.x) + (a.y-c.y)*(a.x+c.x));
}


int main(int argc, char** argv) {
    if (argc<2) {
        std::cerr << "Usage: " << argv[0] << " filename" << std::endl;
        return 1;
    }
    std::cerr << "Checking " << argv[1] << std::endl;

    Triangles m0;
    SurfaceAttributes a0 = read_by_extension(argv[1], m0);
    PointAttribute<vec2> tex0("tex_coord", a0, m0);
    FacetAttribute<double> f(m0);

    const double theta = .5;
    double qual_max0 =  -std::numeric_limits<double>::max();

    vec2 P0 = {std::numeric_limits<double>::max(), -std::numeric_limits<double>::max()};
    vec2 P1 = {-std::numeric_limits<double>::max(), std::numeric_limits<double>::max()};

    for (int t : facet_iter(m0)) {
        vec2 A,B,C;
        m0.util.project(t, A, B, C);
        mat<2,2> ST = {{B-A, C-A}};
        mat<3,2> ref_tri = mat<3,2>{{ {-1,-1},{1,0},{0,1} }}*ST.invert_transpose();

        mat<2,2> J;
        for (int i : {0,1,2})
            for (int d : {0,1}) {
                int v = m0.vert(t,i);
                J[d] += ref_tri[i]*tex0[v][d];
            }

        {
            mat2x2 G = J.transpose() * J;
            mat2x2 evec;
            vec2 eval;
            eigendecompose_symmetric(G, eval, evec);
            f[t] = 1.+std::sqrt(pow(1-std::sqrt(eval.x),2)+pow(1-std::sqrt(eval.y),2));

//          double f0 = (J[0]*J[0] + J[1]*J[1])/(2.*J.det());
//          double g0 = (1+J.det()*J.det())/(2.*J.det());
//          qual_max0 = std::max(qual_max0, ((1.-theta)*f0 + theta*g0));
          qual_max0 = std::max(qual_max0, f[t]);
//          f[t] = ((1.-theta)*f0 + theta*g0);
        }

        mat2x2 G = J.transpose() * J;
        mat2x2 evec;
        vec2 eval;
        eigendecompose_symmetric(G, eval, evec);
        P1.x = std::max(std::sqrt(eval.x), P1.x);
        P1.y = std::min(std::sqrt(eval.y), P1.y);

        P0.x = std::min(J.det(), P0.x);
        double cond = std::sqrt(eval.x/eval.y);
//      std::cout << cond << std::endl;
        P0.y = std::max(cond, P0.y);
    }
  std::cout << qual_max0 << " " << P0 << std::endl;
//  std::cout << P0 << std::endl;
//std::cout << P1 << std::endl;
//std::cout << std::sqrt(P1.x/P1.y) << std::endl;
//std::cout << std::sqrt(P1.x/P1.y) << std::endl;
//    std::cout <<  std::endl;
//  for (int i : range(m0.nverts())) {
//      m0.points[i] = {tex0[i].x, tex0[i].y, 0};
//  }
//  for (int i : range(m0.nfacets())) {
//  for (int j : range(3))
//      m0.points[m0.vert(i,j)].z = f[i]*100;
//  }

    write_by_extension("dump.geogram", m0, SurfaceAttributes{ { {"tex_coord", tex0.ptr} }, {{"f", f.ptr}}, {} });
    return 0;
}

