#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#undef NDEBUG
#include <cassert>
#include <ultimaille/all.h>

using namespace UM;


int main(int argc, char** argv) {
    if (3>argc) {
        std::cerr << "Usage: " << argv[0] << " path/" << std::endl;
        return 1;
    }
    std::cerr << "Checking " << argv[1] << std::endl;


    Tetrahedra ref, unt, opt;
    read_by_extension(argv[1], ref);
    read_by_extension(argv[2], unt);
    read_by_extension(argv[3], opt);

    assert(ref.ncells()==unt.ncells() && ref.ncells()==opt.ncells());

    const double theta = 0.5;

    vec2 P0 = {std::numeric_limits<double>::max(), -std::numeric_limits<double>::max()};
    vec2 P1 = {std::numeric_limits<double>::max(), -std::numeric_limits<double>::max()};

    for (int t : cell_iter(ref)) {
        mat<3,3> ST = {{
            ref.points[ref.vert(t, 1)] - ref.points[ref.vert(t, 0)],
            ref.points[ref.vert(t, 2)] - ref.points[ref.vert(t, 0)],
            ref.points[ref.vert(t, 3)] - ref.points[ref.vert(t, 0)]
        }};
        mat<4,3> ref_tet = mat<4,3>{{ {-1,-1,-1},{1,0,0},{0,1,0},{0,0,1} }}*ST.invert_transpose();


        mat<3,3> J0, J1;
        for (int i : {0,1,2,3})
            for (int d : {0,1,2}) {
                assert(ref.vert(t,i)==unt.vert(t,i) && ref.vert(t,i)==opt.vert(t,i));
                int v = ref.vert(t,i);
                J0[d] += ref_tet[i]*unt.points[v][d];
                J1[d] += ref_tet[i]*opt.points[v][d];
            }

        mat3x3 G0 = J0.transpose() * J0;
        mat3x3 G1 = J1.transpose() * J1;
        mat3x3 evec0, evec1;
        vec3 eval0, eval1;
        eigendecompose_symmetric(G0, eval0, evec0);
        eigendecompose_symmetric(G1, eval1, evec1);

        P0.x = std::min(J0.det(), P0.x);
        P0.y = std::max(std::sqrt(eval0.x/eval0.z), P0.y);
        P1.x = std::min(J1.det(), P1.x);
        P1.y = std::max(std::sqrt(eval1.x/eval1.z), P1.y);
    }

    std::cout << P0 << " " << P1 << std::endl;
//    std::cerr << qual_max0 << " " << qual_max1 << std::endl;


    return 0;
}

