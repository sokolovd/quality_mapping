#!/bin/bash
rm gna3d.csv

for i in `find /mnt/Locally-Injective/ -name  "rest.vtk"`; do
UNTFILE=`dirname $i`/untangle.mesh
OPTFILE=`dirname $i`/stiffening.mesh
echo $i
./quality3d $i $UNTFILE >>gna3d.csv
./quality3d $i $OPTFILE >>gna3d.csv
done

