#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#undef NDEBUG
#include <cassert>
#include <ultimaille/all.h>

using namespace UM;

#define CORNER 0

double triangle_area_2d(vec2 a, vec2 b, vec2 c) {
    return .5*((b.y-a.y)*(b.x+a.x) + (c.y-b.y)*(c.x+b.x) + (a.y-c.y)*(a.x+c.x));
}

double triangle_aspect_ratio_2d(vec2 a, vec2 b, vec2 c) {
    double l1 = (b-a).norm();
    double l2 = (c-b).norm();
    double l3 = (a-c).norm();
    double lmax = std::max(l1, std::max(l2, l3));
    return lmax*(l1+l2+l3)/(4.*std::sqrt(3.)*triangle_area_2d(a, b, c));
}

int main(int argc, char** argv) {
    if (2>argc) {
        std::cerr << "Usage: " << argv[0] << " path/" << std::endl;
        return 1;
    }
//    std::cerr << "Checking " << argv[1] << std::endl;

    Triangles m;
    SurfaceAttributes a = read_by_extension(argv[1], m);
#if CORNER
    CornerAttribute<vec2> tex("tex_coord", a, m);
#else
    PointAttribute<vec2> tex("tex_coord", a, m);
#endif
    FacetAttribute<double> logD(m);
    FacetAttribute<double> D(m);
    FacetAttribute<double> T(m);

    vec2 P = {std::numeric_limits<double>::max(), -std::numeric_limits<double>::max()};

    double qual_max = -std::numeric_limits<double>::max();
    double smax = -std::numeric_limits<double>::max();
    double smin =  std::numeric_limits<double>::max();

    double max_aspect_ratio = -std::numeric_limits<double>::max();
    for (int t : facet_iter(m)) {
        vec2 A,B,C;
        m.util.project(t, A, B, C);
        max_aspect_ratio = std::max(max_aspect_ratio, triangle_aspect_ratio_2d(A,B,C));
        mat<2,2> ST = {{B-A, C-A}};
        mat<3,2> ref_tri = mat<3,2>{{ {-1,-1},{1,0},{0,1} }}*ST.invert_transpose();

        mat<2,2> J;
        for (int i : {0,1,2})
            for (int d : {0,1}) {
#if CORNER
                J[d] += ref_tri[i]*tex[m.corner(t,i)][d];
#else
                J[d] += ref_tri[i]*tex[m.vert(t,i)][d];
#endif
            }
        {
            constexpr double theta = 0.5;
            double f = (J[0]*J[0] + J[1]*J[1])/(2.*J.det());
            double g = (1+J.det()*J.det())/(2.*J.det());
            qual_max = std::max(qual_max, ((1.-theta)*f + theta*g));
        }

        mat2x2 G = J.transpose() * J;
        mat2x2 evec;
        vec2 eval;
        eigendecompose_symmetric(G, eval, evec);

        double x = J.det();
        double s1 = std::sqrt(eval.x);
        double s2 = std::sqrt(eval.y);
        double y = s1/s2;
        P.x = std::min(x, P.x);
        P.y = std::max(y, P.y);

        smax = std::max(smax, s1);
        smin = std::min(smin, s2);

//      std::cerr << y << "," << x << std::endl;

        D[t] = x;
        logD[t] = std::log(x)/std::log(10);
        T[t] = y;

//        max_aspect_ratio = std::max(max_aspect_ratio, triangle_aspect_ratio_2d(tex[m.corner(t,0)], tex[m.corner(t,1)], tex[m.corner(t,2)]);
    }

/*
#if CORNER
    std::vector<vec3> copy = *(m.points.data);
    CornerAttribute<vec3> orig(m);

    m.points.resize(m.ncorners());
    for (int c : corner_iter(m)) {
        for (int d : {0,1})
            m.points[c][d] = tex[c][d];
        m.points[c][2] = 0;
        orig[c] = copy[m.vert(c/3, c%3)];
    }
    for (int t : facet_iter(m))
        for (int v : {0,1,2})
            m.vert(t,v) = m.corner(t, v);
    write_geogram("attr.geogram", m, {{}, {{"det", D.ptr}, {"logdet", logD.ptr}, {"t", T.ptr}}, {}});

    { // colocate vertices
        std::vector<int> old2new;
        colocate(*m.points.data, old2new, 0);
        for (int t : facet_iter(m))
            for (int lv : range(3))
                m.vert(t, lv) = old2new[m.vert(t, lv)];
   }

    m.delete_isolated_vertices();
    write_geogram("attr2.geogram", m, {{}, {{"det", D.ptr}, {"logdet", logD.ptr}, {"t", T.ptr}}, {}});

    PointAttribute<vec2> tex2(m);
    for (int c : corner_iter(m)) {
        tex2[m.vert(c/3,c%3)] =  tex[c];
        m.points[m.vert(c/3, c%3)] = orig[c];
    }
    write_geogram("attr3.geogram", m, {{{"tex_coord", tex2.ptr}}, {{"det", D.ptr}, {"logdet", logD.ptr}, {"t", T.ptr}}, {}});


#else
    write_geogram("attr3d.geogram", m, {{}, {{"det", D.ptr}, {"logdet", logD.ptr}, {"t", T.ptr}}, {}});
    for (int v : vert_iter(m)) {
        for (int d : {0,1})
            m.points[v][d] = tex[v][d];
        m.points[v][2] = 0;
    }
    write_geogram("attr.geogram", m, {{}, {{"det", D.ptr}, {"logdet", logD.ptr}, {"t", T.ptr}}, {}});
#endif
*/
    for (int v : vert_iter(m)) {
        for (int d : {0,1})
            m.points[v][d] = tex[v][d];
        m.points[v][2] = 0;
    }
    write_geogram("attr.geogram", m, {{}, {{"cond", T.ptr}}, {}});

    std::cout << P << std::endl;
//  std::cout << "qual_max: " << qual_max << std::endl;
//  std::cout << "sqrt(smax/smin): " << std::sqrt(smax/smin) << std::endl;
//  std::cout << "max input aspect ratio: " << max_aspect_ratio << std::endl;


    return 0;
}

