#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#undef NDEBUG
#include <cassert>
#include <ultimaille/all.h>

using namespace UM;
double triangle_area_2d(vec2 a, vec2 b, vec2 c) {
    return .5*((b.y-a.y)*(b.x+a.x) + (c.y-b.y)*(c.x+b.x) + (a.y-c.y)*(a.x+c.x));
}


int main(int argc, char** argv) {
    if (2>argc) {
        std::cerr << "Usage: " << argv[0] << " path/" << std::endl;
        return 1;
    }
    std::cerr << "Checking " << argv[1] << std::endl;

    Triangles m0, m1;
    SurfaceAttributes a0 = read_by_extension(argv[1]+std::string("/input.obj"), m0);
    SurfaceAttributes a1 = read_by_extension(argv[1]+std::string("/result_LBD_t0.obj"), m1);
    PointAttribute<vec2> tex0("tex_coord", a0, m0);
    for (int c : vert_iter(m0))
        tex0[c] = {m1.points[c].x, m1.points[c].y};

    write_by_extension(argv[1]+std::string("/lbd.obj"), m0, SurfaceAttributes{ { {"tex_coord", tex0.ptr} }, {}, {} });

    return 0;
}

